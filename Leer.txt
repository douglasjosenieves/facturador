1 Add the following class, to "providers" array in the file config/app.php
crocodicstudio\crudbooster\CRUDBoosterServiceProvider::class,

2 Setting the database configuration, open .env file at project root directory
DB_DATABASE=**your_db_name**
DB_USERNAME=**your_db_user**
DB_PASSWORD=**password**
Run the following command at the terminal


3 $ php artisan crudbooster:install

4 Backend URL
/admin/login

default email : admin@crudbooster.com
default password : 123456