<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');



Route::get('contactos','contactosController@Index');
Route::get('contactos/set/', 'contactosController@setContactos');
Route::get('contactos/buscar/', 'contactosController@buscarContactos');




Route::get('articulos/buscar/', 'articulosController@buscarArticulos');
Route::get('articulos/set/', 'articulosController@setArticulos');

Route::get('articulos_categorias', 'articulosController@getCategorias');

Route::get('orders-set', 'ordersController@setOrden');
Route::get('orders-set/getid', 'ordersController@getId');


Route::get('orders-set-fv', 'ordersFvController@setOrden');
Route::get('orders-set-fv/getid', 'ordersFvController@getId');


Route::get('orders-set-cotizaciones', 'ordersCotizacionesController@setOrden');
Route::get('orders-set-cotizaciones/getid', 'ordersCotizacionesController@getId');


Route::get('orders-set-compras', 'ordersComprasController@setOrden');
Route::get('orders-set-compras/getid', 'ordersComprasController@getId');


Route::get('inventario-set', 'inventariosController@setOrden');

Route::get('vendedor/buscar/', 'vendedorController@buscar');
Route::get('cuentas/buscar/', 'cuentasController@buscar');

Route::get('cuentas-set', 'cuentasController@setCuenta');