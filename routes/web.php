<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 

Route::group(['middleware' => ['guest']], function () {

    //Router for custom api defeault

Route::get('/', function () {
    //return redirect('/admin');

   return view('welcome');
});

Route::get('/pdf/{id}', function($id) {

  $data = [];
  $data['page_title'] = 'Detail Data';
  $data['row'] = DB::table('orders_fv')->join('contactos', 'contactos_id', '=', 'contactos.id')
->join('vendedor', 'vendedor_id', '=', 'vendedor.id')

  ->where('orders_fv.id',$id)->select('orders_fv.*', 'vendedor.name as ven_name',  'contactos.name as name', 'contactos.documento as rif', 'contactos.direccion',  'contactos.telefono')->first();

$pdf = PDF::loadView('pdf.factura-detalle', $data);
return $pdf->download('factura-'.$id.'.pdf');
});





Route::get('index', function () {
    //return redirect('/admin');

   return view('welcome');
})->name('index');



Route::get('cotizacion', function() {
    return view('cotizacion');
})->name('cotizacion');;

Route::get('compra', function() {
    return view('compra');
})->name('compra');;


Route::get('inventario', function() {
    return view('inventario');
})->name('inventarios');;

Route::get('cuentas', function() {
    return view('cuentas');
})->name('cuentas');;
});
 