<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
 


})->describe('Display an inspiring quote');


Artisan::command('reset', function () {
$this->info("CB APPS INSTALLATION");
Schema::disableForeignKeyConstraints();

	foreach(DB::select('SHOW TABLES') as $table) {
	    $table_array = get_object_vars($table);
	    Schema::drop($table_array[key($table_array)]);
	}

	Schema::enableForeignKeyConstraints();

	$this->info('Reinstalando...');
	$this->call('crudbooster:update');

 
});



Artisan::command('gea', function () {
  $this->info('Cargando datos...');
	$this->call("db:seed",["--class"=>"DatabaseSeeder"]);

	});