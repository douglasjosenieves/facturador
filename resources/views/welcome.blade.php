<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Facturación</title>
      

 
        <link rel="stylesheet" href="{{ asset('css/app.css') }}" >
    
 

      
    </head>

    <style type="text/css">
      
      @media print {
@page  
{ 
    size: auto;   /* auto is the initial value */ 

    /* this affects the margin in the printer settings */ 
    margin: 3mm auto auto auto;  
} 
}
    </style>
<body>
        <div class="container" id="vue">
 
 
       
    <form id="formu" action="" method="GET" role="form"  @submit.prevent="guardarOrder">
 


<div class="hidden-print">

 <input type="checkbox" v-model="show_control" value="true">
        Panel de control
        <br>
@include('nav')





</div>
  <div class="panel panel-success hidden-print" v-if="show_control">
      <div class="panel-heading">
          <h3 class="panel-title">Panel de control</h3>
      </div>
      <div class="panel-body">
       
<div class="row">
        <div class="col-md-2">
      <label>Documento:</label>  
<select name="documento" v-model="documento" @change="verificaId()" :disabled="contactos_id!=''" id="input" class="form-control" required="required">

<option value="Factura de Venta">Factura de Venta</option>

</select>

        </div>
        <div class="col-md-2">
         <label>Fecha de emisión:</label>
<input type="date" name="fecha_emi" readonly v-model="fecha_emi" id="inputFecha_emi" class="form-control" value="" required="required" title="">

        </div>
  
    <div class="col-md-2">
         <label>Vence:</label>
<input type="date" name="vence" v-model="vence" id="vence" class="form-control" value="" required="required" title="">

        </div>


                        <div class="col-md-2">
            
      <label>Vendedor:</label>  
<select name="vendedors" v-model="vendedors" id="input" class="form-control" required="required">
<option value="">Vendedor</option>
<option v-for="item in vendedor" v-bind:value="item.id" >@{{item.name}}</option>
 

</select>
  </div>

               <div class="col-md-1">
            
      <label>Iva:</label>  
<select name="moneda" v-model="tax" id="input" class="form-control" required="required">
<option value="12">12</option>
<option value="9">9</option>
<option value="7">7</option>
 <option value="0">0</option>

</select>

        </div>

                <div class="col-md-1">
            
      <label>Moneda:</label>  
<select name="moneda" v-model="moneda" id="input" class="form-control" required="required">
<option value="Bs">Bs</option>
<option value="$">$</option>
<option value="€">€</option>
 

</select>

        </div>

       <div class="col-md-2">
            
      <label>Config Adicionales:</label>  <br>
      Empresa
        <input type="checkbox" v-model="show_empresa" value="true">
     Cuentas
        <input type="checkbox" v-model="show_ctas" value="true">
    Logo
        <input type="checkbox" v-model="show_logo" value="true">
   
      </div>
        </div>

<br>
    
        <div class="row">

<div class="col-md-3">
    <label>Rif:</label>
    <span class="label label-success" v-if="contactos_id">Buen trabajo <i class="glyphicon glyphicon-check"></i> </span> 
 
<input type="text" autocomplete="off" name="documento" id="inputdocumento" @keyup.prevent="buscarDoc" class="form-control" value="" v-model="docBuscar" placeholder="Rif"    title="">
  

</div>



<div class="col-md-3">

<input type="hidden" v-model="contactos_id" name="contactos_id">
    <label>Dirigido a Nombre:</label>  



<input type="text" :disabled="contactos_id != ''" name="name" autocomplete="off" id="inputName" @keyup.prevent="buscarContactos" class="form-control" value="" v-model="name" placeholder="Dirigido a Nombre" title="">
  
<ul class="list-group" v-if="panelb">
  <li style="cursor: pointer;" class="list-group-item" v-for="item in contactosBuscados" @click="selectContacto( item.id, item.name, item.direccion, item.telefono, item.documento)">@{{item.id}} - @{{item.name}} - @{{item.documento}}</li>
 
</ul>

</div>
<div class="col-md-3">
      <label>Dirección:</label>  

 <input type="text" name="direc" id="inputDirec" v-model="direc" disabled= placeholder="Dirección" class="form-control" value="" required="required" pattern="" title="">

</div>
<div class="col-md-3">
      <label>Teléfono:</label> 

      <input type="tel" name="tel" id="inputTel" class="form-control" placeholder="Teléfono" disabled value="" v-model="tel" required="required" title="">

</div>

 
</div>

<div class="row">
  
  <div class="col-md-8">
    
      <label>Condiciones:</label> 

<input type="text" name="detale" id="inputdetale" class="form-control" placeholder="Condiciones"  value="" v-model="opcional_enc"   title="">

</div>


  <div class="col-md-3">
      <label>Nº Control:</label> 

      <input type="text" name="ontrol" id="Controle" class="form-control" placeholder="Nº Control"  value="" v-model="opcional1_enc"  required="required"   title="">

</div>
</div>
<br>
 

      </div>
  </div>
    <div class="row">
      <img v-if="show_logo" src="{{ asset('images/GEAEXPRES-LOGO.png') }}" class="pull-right"  alt="">
        <div class="col-xs-12">

            <div   class="invoice-title">

            <?php 
//$stamp = date("ymdhs");
//$rando = mt_rand(0,9); 


$order_number  = str_pad(0, 4, 0 , STR_PAD_LEFT);
$order_number .= $stamp.$rando;

             ?>
    <span class="hidden" id="order_numero">{{$order_number}}@{{fvid}}</span>
                <h2 class="hidden-print">@{{documento}}</h2>

            
                 
            </div>
            <hr class="hidden-print">
            <div class="row text-center">
                <div class="col-xs-12">
                    <address>
                    <strong>@{{documento}} a:</strong><br>
                      <strong>  RIF: </strong>@{{docBuscar}}<br>
                   <strong> Nombre o Razón social:</strong> @{{name}}<br>
                        <strong> Dirección Fiscal:</strong> @{{direc}}<br>
                       <strong> Teléfono:</strong> @{{tel}}<br>
                   <strong> <span v-if="documento=='Factura de Venta'">Factura</span>  Nº {{$order_number}}@{{fvid}}</strong>

                    </address>
               <div class="col-xs-6 "> <strong>Fecha:</strong><br>
                        @{{fecha_emi}}<br><br></div>
                  <div class="col-xs-6">   
                        <strong>Vence:</strong><br>
                        @{{vence}}<br><br></div>

     <div  v-if="show_empresa" class="">
                    <address>
                    <strong>@{{documento}} de:</strong><br>
                        @{{empresa['name']}} @{{empresa['rif']}}<br>
                        @{{empresa['direc']}}<br>
                        @{{empresa['tel']}}<br>
                   
                    
                    </address>
                </div>


                </div>

                


           


            </div>
            <div class="row">
                <div  class="col-xs-9">
                    <address v-if="show_ctas">
                        <strong>Nº de ctas para emitir pagos:</strong><br>
                        @{{banco_responsable.name}} | @{{banco_responsable.doc}} <br>
                    @{{banco_responsable.email}}
                       <span v-for="item in ctas_bancarias"><br><strong>@{{item.banco}}</strong> @{{item.numero}} @{{item.tipo}}</span> 
                    </address>
<br>
                  <p  v-if="opcional_enc" > <strong>Condiciones:</strong> @{{opcional_enc}}</p> 

                </div>
                <div class="col-xs-3 text-right">
                <div class="row">
                  

                  
                </div>
                   
                       
              

                  
                    
                </div>
            </div>
        </div>
    </div>
    
{{--  v-if="contactos_id" --}}
<div class="panel panel-info  hidden-print"  v-if="contactos_id">
  <div class="panel-heading">
    <h3 class="panel-title">Agregar items</h3>
  </div>
  <div class="panel-body">
               <div class="row" >
    
<div class="col-md-3">   
<label>Descripción:</label>  
<input type="text" name="" id="input" @keyup.prevent="buscarArticulos" v-model="items" class="form-control" value="" autocomplete="off"  title="" placeholder="Descripción">
<ul class="list-group" v-if="panelc">
  <li style="cursor: pointer;" class="list-group-item" v-for="item in articulosBuscados" @click="selectArticulos( item.id, item.name, item.precio, item.sku, item.tax, item.precio_compra )">@{{item.sku}} - @{{item.name}} - PVP@{{item.precio}} - STOCK@{{item.stock}}</li>
 
</ul>
</div>

<input type="hidden" name="items_id" v-model="items_id" >
<input type="hidden" name="items_sku" v-model="items_sku" >

<div class="col-md-3">
<label>Precio:</label>  
<input type="number" disabled="" step="0.01"  name="" id="input" v-model="precio"  class="form-control" value=""  title="">

</div>
<div class="col-md-1">    
<label>Cantidad:</label> 
<input type="number"  name="" id="input" @change.prevent="verificaStock()" v-model="cantidad" class="form-control" value="" min="1"  title="">

</div>

<div class="col-md-1">
 <label>Iva:</label>    
<input type="number" name="" id="input"  disabled v-model="tax_i" class="form-control" value=""  title="">

</div>

 

<div class="col-md-2">
 <label>Total:</label>    
<input type="text" name="" id="input" disabled v-model="total" class="form-control" value=""  title="">
 
</div>





<div class="col-md-1">
 <label>Acción:</label><br>    
<button type="button" v-bind:disabled="isButtonDisabled"   class="btn btn-success" @click="agregar"> <i class="glyphicon-plus"></i> Agregar</button>

</div>

 
</div>
  </div>
</div>


    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Resumen</strong></h3>
                </div>
                <div class="panel-body">
  

                  <div class="table-responsive">
                        <table class="table table-condensed" id="table-orderdetail">
                            <thead>
                                <tr>
                                  <td><strong>Código</strong></td>
                                    <td><strong>Descripción</strong></td>
                                                <td class="text-center"><strong>Cantidad</strong></td>
                                    <td class="text-center"><strong>Precio</strong></td>
                        
                               
                                    <td class="text-right"><strong>Total Items</strong></td>
                                    <td width="10px" class="text-right"><strong>&nbsp;</strong></td>
                                     
                                </tr>
                            </thead>
                            <tbody>
                                <!-- foreach ($order->lineItems as $line) or some such thing here -->
                                

<tr v-for="item, index in row">
<td>@{{item.items_sku}}</td>
<td> @{{item.name}}  </td>
<td class="text-center">@{{item.cantidad}}</td>
<td class="text-center">@{{moneda}} @{{item.precio}}</td>
<td class="text-right">@{{moneda}} <span class="">@{{ item.total }} </span></td>
<td class="text-right"><button v-if="show_control" type="button" @click="removeRow(index)" class="hidden-print btn btn-danger btn-xs"> Eliminar </button></td>
                                </tr>
                          
 


                       
                                <tr>
                                    <td class="thick-line"></td>
                                    <td class="thick-line"></td>
                                    <td class="thick-line"></td>
                                    <td class="thick-line text-center"><strong>Subtotal</strong></td>
                                    <td class="thick-line text-right ">@{{moneda}}  @{{totalSub}} 
                        
                                    <td class="thick-line"></td>
                                </tr>
                   

                                <tr>
                                    <td class="thick-line"></td>
                                    <td class="thick-line"></td>
                                    <td class="thick-line"></td>
                                    <td class="thick-line text-center"><strong>IVA @{{tax}}%</strong></td>
                                    <td class="thick-line text-right ">@{{moneda}} @{{totaliva}}  </td>
                                    <td class="thick-line"></td>
                                </tr>
                   

                                <tr>
                                    <td class="no-line"></td>
                                       <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line text-center"><strong>Total</strong></td>
                                    <td class="no-line text-right"><strong>@{{moneda}} @{{totalgrand}} </td>

                                     <input type="hidden" id="granTotal" v-model="totalgrand" name="">
                                </tr>



                            </tbody>
                        </table>


                    </div>


                </div>

            </div>
            <div v-if="guardar">
            


 <button @click="guardarComo('guardarynuevo')" style="margin-left: 10px" id="gn"  type="submit" class="btn btn-success pull-right hidden-print" v-bind:disabled="btn_guardar"><span class="glyphicon glyphicon-save"></span> Guardar y Nuevo</button>   


 <button @click="guardarComo('guardaryimprimir')"  type="submit" id="gi" class="btn btn-primary pull-right hidden-print" v-bind:disabled="btn_guardar"><span class="glyphicon glyphicon-print"></span> Guardar e Imprimr</button>

            </div>
 <div v-else="guardar">

<div class="alert alert-success hidden-print">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <strong>@{{documento}} fue guardada con exito! </strong> Precione F5 para crear uno nuevo, o <a target="_blank" href="admin" class="btn btn-info btn-xs">Ir al Admin</a>
</div>

 </div>

   


         
                  
        </div>
    </div>


</form>

 




 

</div> <!--vue fin -->


  




 


<script type="text/javascript" src="{{ asset('js/factura-ventas.js') }}"></script>
        
    </body>
</html>
