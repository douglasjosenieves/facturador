<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>GeaLight</title>
      

 
        <link rel="stylesheet" href="{{ asset('css/app.css') }}" >
    
 

      
    </head>
<body>
        <div class="container" id="vue">

 
       
 


<div class="hidden-print">

 <input type="checkbox" v-model="show_control" value="true">
        Panel de control
        <br>
<div class="pull-right">
<a class="btn btn-info" href="{{ url('admin') }}" role="button">GeaLight Admin</a>
<span class="label label-warning">Vista previa de la {{$row->documento}}</span>


</div>





</div>
  <div class="panel panel-default hidden-print" v-if="show_control">
      <div class="panel-heading">
          <h3 class="panel-title">Panel de control</h3>
      </div>
      <div class="panel-body">
       
<div class="row ">
        <div class="col-md-3">

   
      <label>Documento: </label>  
<select name="documento"  id="input" readonly="true" class="form-control" required="required">
 
 
<option value="{{$row->documento}}" selected="selected" >{{$row->documento}}</option>
</select>

        </div>
        <div class="col-md-2">
         <label>Fecha de emisión:</label>
<input type="text" name="fecha_emi" readonly="true" id="inputFecha_emi" class="form-control" value="{{$row->created_at}}" required="required" title="">

        </div>
        <div class="col-md-2 hidden">
            
      <label>Tax:</label>  
<select name="tax"  readonly="true" rea id="input" class="form-control" required="required">
<option value="{{$row->tax}}">{{$row->tax}}</option>
 

</select>

        </div>


                <div class="col-md-2 hidden">
            
      <label>Moneda:</label>  
<select name="moneda" readonly="true"  id="input" class="form-control" required="required">
<option value="{{$row->moneda}}">{{$row->moneda}}</option>
 
 

</select>

        </div>

       <div class="col-md-3 hidden">
            
      <label>Config Adicionales:</label>  <br>
      Empresa
        <input type="checkbox" v-model="show_empresa" value="true">
     Cuentas
        <input type="checkbox" v-model="show_ctas" value="true">
    Logo
        <input type="checkbox" v-model="show_logo" value="true">
   
      </div>
        </div>

<br>
    
        <div class="row hidden">

<div class="col-md-3">
    <label>Documento:</label>
    <span class="label label-success" v-if="contactos_id">Buen trabajo <i class="glyphicon glyphicon-check"></i> </span> 
 
<input type="text" readonly="true" name="documento" id="inputdocumento" @keyup.enter.prevent="buscarDoc" class="form-control" value="{{$row->rif}}"  placeholder="Enter para buscar" required="required"   title="">
 

</div>



<div class="col-md-3">

<input type="hidden" v-model="contactos_id" name="contactos_id">
    <label>Dirigido a Nombre:</label>  
<input type="text" name="name" readonly="true" autocomplete="off" id="inputName" @keyup.enter.prevent="buscarContactos" class="form-control" value="{{$row->name}}" placeholder="Enter para buscar" required="required" pattern="" title="">
 
 

</div>
<div class="col-md-3">
      <label>Dirección:</label>  

 <input type="text" name="direc" id="inputDirec"  readonly="true" placeholder="Dirección" class="form-control" value="{{$row->direccion}}" required="required" pattern="" title="">

</div>
<div class="col-md-3">
      <label>Teléfono:</label> 

      <input type="tel" name="tel" id="inputTel" class="form-control" placeholder="Teléfono" readonly="true" value="{{$row->telefono}}"  required="required" title="">

</div>

 
</div>
<br>


      </div>
  </div>
    <div class="row container">
      <img v-if="show_logo" src="{{ asset('images/GEAEXPRES-LOGO.png') }}" class="pull-right"  alt="">
        <div class="col-xs-12">

            <div class="invoice-title">

            <?php 
$stamp = date("ymdhs");
$rando = mt_rand(0,9); 
$order_number  = str_pad(0, 1, 0 , STR_PAD_RIGHT);
$order_number .= $stamp.$rando;

             ?>
    <span class="hidden" id="order_numero">{{$order_number}}</span>
                <h2>Ajuste de {{$row->documento}}</h2><h3 class="pull-right"># {{$row->order_numero}} </h3>

                 <h3> <strong>Concepto:</strong> {{$row->descripcion}}</h3>

             
            </div>
            <hr>
            <div class="row">
                <div class="col-xs-6">
                 
                </div>
            



            </div>
    
        </div>
    </div>
    

<div class="panel panel-info  hidden-print" v-if="contactos_id">
  <div class="panel-heading">
    <h3 class="panel-title">Agregar items</h3>
  </div>
  <div class="panel-body">
               <div class="row" >
    
<div class="col-md-3">   
<label>Descripción:</label>  
<input type="text" name="" id="input" @keyup.enter.prevent="buscarArticulos" v-model="items" class="form-control" value="" required="required" pattern="" title="" placeholder="Enter para buscar">
<ul class="list-group" v-if="panelc">
  <li style="cursor: pointer;" class="list-group-item" v-for="item in articulosBuscados" @click="selectArticulos( item.id, item.name, item.precio, item.sku)">@{{item.sku}} - @{{item.name}} - @{{item.precio}}</li>
 
</ul>
</div>
<input type="hidden" name="items_id" v-model="items_id" >
<input type="hidden" name="items_sku" v-model="items_sku" >

<div class="col-md-2">
<label>Precio:</label>  
<input type="number" step="0.01"  name="" id="input" v-model="precio" readonly="true" class="form-control" value="" required="required" pattern="" title="">

</div>
<div class="col-md-2">    
<label>Cantidad:</label> 
<input type="number" step="0.01" name="" id="input" v-model="cantidad" class="form-control" value="" required="required" pattern="" title="">

</div>
<div class="col-md-2">
 <label>Total:</label>    
<input type="text" name="" id="input" readonly="true" v-model="total" class="form-control" value="" required="required" pattern="" title="">

</div>

<div class="col-md-2">
 <label>Acción:</label><br>    
<button type="button" v-bind:disabled="isButtonDisabled"   class="btn btn-success" @click="agregar"> <i class="glyphicon-plus"></i> Agregar</button>

</div>

 
</div>
  </div>
</div>


    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Resumen @{{documento}}</strong></h3>
                </div>
                <div class="panel-body">
  

                  <div class="table-responsive">
                        <table class="table table-condensed" id="table-orderdetail">
                            <thead>
                                <tr>
                                 <td><strong>Id</strong></td>
                                    <td><strong>Descripción</strong></td>
                                    
                                    <td class="text-center"><strong>Cantidad</strong></td>
                                    <td class="text-right"><strong>Total</strong></td>
                                    <td width="10px" class="text-right"><strong>&nbsp;</strong></td>
                                     
                                </tr>
                            </thead>
                            <tbody>
                                <!-- foreach ($order->lineItems as $line) or some such thing here -->
        <?php 

//echo $row->detalle;


        $items = json_decode( $row->detalle);  ?>                       
 
@foreach ($items as $item)
  

 
                                <tr >
                                <td>{{$item->items_sku}}</td>
                                    <td>{{$item->name}}</td>

                                    
                                    <td class="text-center">{{$item->cantidad}}</td>
                                    
                                    <td class="text-right">{{$row->moneda}}<span class="sub_total">{{$item->total}}</span></td>
                                   <td class="text-right"></td>
                                </tr>
                          
 
@endforeach


                       
                                <tr>
                                    <td class="thick-line"></td>
                                    <td class="thick-line"></td>
                                    <td class="thick-line text-center"><strong>Subtotal</strong></td>
                                    <td class="thick-line text-right ">@{{moneda}}<span class="sub_total_total">0</span></td>
                                    <td class="thick-line"></td>
                                </tr>





                                <tr class="hidden" >
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line text-center" ><strong>Tax <span id="taxe">{{$row->tax}}</span>%</strong></td>
                                    <td class="no-line text-right">{{$row->moneda}}<span id="tax">{{$row->tax_total}}</span></td>


                                </tr>
                                <tr>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line text-center"><strong>Total</strong></td>
                                    <td class="no-line text-right"><strong>@{{moneda}}<span id=""> 
                                     {{$row->grand_total}}</span></strong></td>

                                     <input type="hidden" id="granTotal" v-model="gran_total" name="">
                                </tr>
                            </tbody>
                        </table>


                    </div>


                </div>

            </div>
            <div v-if="guardar">
   
            </div>
 <div v-else="guardar">

<div class="alert alert-success">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <strong>@{{documento}} fue guardada con exito! </strong> Precione F5 para crear uno nuevo, o <a target="_blank" href="admin/orders" class="btn btn-info btn-xs">Ir al Admin</a>
</div>

 </div>

        
            
                  
        </div>
    </div>




@include('create_contactos');

@include('create_articulos');



 

</div> <!--vue fin -->


  





 


<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
        
    </body>
</html>
