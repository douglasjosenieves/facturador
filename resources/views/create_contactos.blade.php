
<!-- Large modal contactos-->
 

<div class="modal fade modal-contactos" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
   
<div class="" style="padding:30px">
<div class="panel panel-success">
  <div class="panel-heading">
    <h3 class="panel-title"><span class="glyphicon glyphicon-user"></span>Crear contacto  </h3>
  </div>
  <div class="panel-body">


<form action="" method="GET" role="form"  @submit.prevent="setContactos">
  <legend>Crear contacto</legend>

<div class="row">
  <div class="col-md-3">
      <label>Tipo:</label>
<select name="tip_rego" id="inputTipo" v-model="tipo_reg"  class="form-control" required="required">
  <option value="">Seleccione</option>
    <option value="Cliente">Cliente</option>
      <option value="Proveedor">Proveedor</option>
</select>
</div>

  <div class="col-md-4">
    <label>Documento:</label>
<input type="text" placeholder="Nit, Rif, Cedula" name="documento" id="inputDoc" class="form-control"  v-model="doc_reg" required="required"  title="">
</div>

<div class="col-md-5">
    <label>Empresa:</label>  
<input type="text" name="name" id="inputName" class="form-control" value="" v-model="name_reg" placeholder="Empresa" required="required"   title="">

</div>

</div>

<br>
        <div class="row">

<div class="col-md-12">
      <label>Dirección:</label>  

 <input type="text" name="direccion" id="inputDirec" v-model="direc_reg" placeholder="Dirección Fiscal" class="form-control" value="" required="required"  title="">

</div>

</div>

<br>


<div class="row">
  
  <div class="col-md-4">
      <label>Teléfono:</label> 

      <input type="tel" placeholder="Teléfono" name="telefono" id="inputTel" class="form-control" value="" v-model="tel_reg" required="required" title="">

</div>


<div class="col-md-4">
      <label>Email:</label> 

      <input type="email" name="email" placeholder="email" id="inputTel" class="form-control" value="" v-model="email_reg" required="required" title="">

</div>

<div class="col-md-4">
      <label>Codigo Sica:</label> 

      <input type="text" placeholder="Codigo Sica" name="opcional" id="inputTel" class="form-control" value="" v-model="opcional_contacto"  title="">

</div>
 <input type="hidden" value=""  v-model="token_reg" name="token">
</div>
<br>
<hr>

<div class="row">
  
  <div class="col-md-4">
      <label>Contato Nombre:</label> 

      <input type="text" placeholder="Contato Nombre" name="telefono" id="inputTel" class="form-control" value="" v-model="contacto_nombre" required="required" title="">

</div>

  <div class="col-md-4">
      <label>Contato Teléfono:</label> 

      <input type="tel" placeholder="Teléfono" name="telefono" id="inputTel" class="form-control" value="" v-model="contacto_telefono"  title="">

</div>


<div class="col-md-4">
      <label>Contato Email:</label> 

      <input type="email" name="email" placeholder="email" id="inputTel" class="form-control" value="" v-model="contacto_email"  title="">

</div>
</div>

<br>
<div class="row">
  

<div class="col-md-10">
      <label>Contacto detalles:</label> 

      <input type="text" name="email" placeholder="email" id="inputTel" class="form-control" value="" v-model="contacto_detalles"  title="">

</div>

</div>







  

  <button  type="submit" class="btn btn-primary pull-right">Crear</button>
</form>
  </div>
</div>
</div>


    </div>
  </div>
</div>



