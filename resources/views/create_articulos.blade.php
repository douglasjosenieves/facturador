


<!-- Small modal articulos -->
 <div class="modal fade modal-articulos" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
   
<div class="" style="padding:30px">
<div class="panel panel-success">
  <div class="panel-heading">
    <h3 class="panel-title"><span class="glyphicon glyphicon-list"></span> Crear artículos  </h3>
  </div>
  <div class="panel-body">



<form action="" method="GET" role="form"  @submit.prevent="setArticulos">
  <legend>Crear artículos</legend>

<div class="row">
  <div class="col-md-4">
    <label>Código sku:</label>
<input type="text" placeholder="Código sku" name="create_art_sku" id="create_art_sku" class="form-control"  v-model="create_art_sku" required="required"  title="">
</div>

<div class="col-md-8">
    <label>Nombre del articulo:</label>  
<input type="text"  name="create_art_name" id="create_art_name" class="form-control" value="" v-model="create_art_name" placeholder="Nombre" required="required"   title="">

</div>

</div>

<br>
        <div class="row">

<div class="col-md-3">
      <label>Descripción:</label>  

 <input type="text" name="create_art_descripcion" id="create_art_descripcion" v-model="create_art_descripcion" placeholder="Descripcion" class="form-control" value="" required="required"  title="">

</div>
<div class="col-md-3">
      <label>Precio de Venta:</label> 

      <input type="number" step="any"  placeholder="0" name="create_art_precio" id="create_art_precio" class="form-control" value="" v-model="create_art_precio" required="required" title="">

</div>

<div class="col-md-3">
      <label>Precio de Compra:</label> 

      <input type="number" step="any" placeholder="0" name="create_art_precio" id="create_art_precio" class="form-control" value="" v-model="create_art_precio_compra" required="required" title="">

</div>

<div class="col-md-3">
      <label>Categoría:</label>  

    
    <select name="create_art_articulos" v-model="create_art_categoria" id="inputCreate_art_articulos" class="form-control" required="required">
     <option value="">Seleccione</option>
      <option v-for="item in articulosCategorias" v-bind:value="item.id" >@{{item.name}}</option>
    </select>

</div>
 <input type="hidden" value=""  v-model="token_reg" name="token">
</div>

<hr>
  
  <div class="row">
    
<div class="col-md-3">
      <label>Tax u/o Iva:</label> 

      <input type="number" step="any" placeholder="0" name="tax" id="tax" class="form-control" value="" v-model="create_art_tax" required="required" title="">

</div>
<div class="col-md-3">
      <label>Grupo:</label> 

      <input type="text" step="any" placeholder="Opcional" name="adicional" id="adicional" class="form-control" value="" v-model="create_art_opcional"   title="">

</div>

<div class="col-md-3">
      <label>Unidad de Medida.</label> 

      <input type="text" step="any" placeholder="Opcional1" name="adicional" id="adicional" class="form-control" value="" v-model="create_art_opcional1"   title="">

</div>

  </div>

  <button  type="submit" class="btn btn-primary pull-right">Crear</button>
</form>
  </div>
</div>
</div>


    </div>
  </div>
    </div>