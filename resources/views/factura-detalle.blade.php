<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>GeaLight</title>
      

 
        <link rel="stylesheet" href="{{ asset('css/app.css') }}" >
    
 

      
    </head>

        <style type="text/css">
      
      * {
font-family: 'arial';

      }
      @media print {
@page  
{ 
    size: auto;   /* auto is the initial value */ 

    /* this affects the margin in the printer settings */ 
    margin: 3mm auto auto auto;  
} 
}

</style>
<body>
        <div class="container" id="vue">

 
      <a style="margin-left:10px" class="btn btn-info hidden-print pull-right" href="{{ url('admin/facturas') }}" role="button">Ver Facturas</a> 
      <a class="btn btn-info hidden-print pull-right" href="{{ url('/pdf/'.$row->id) }}" role="button">   Exportar a PDF</a> 


  
    <div class="row">
      
        <div class="col-xs-12">

            <div class="invoice-title">

            <?php 
$stamp = date("ymdhs");
$rando = mt_rand(0,9); 
$order_number  = str_pad(0, 1, 0 , STR_PAD_RIGHT);
$order_number .= $stamp.$rando;

             ?>


 <span class="hidden" id="order_numero">{{$order_number}}</span>


   
         

 
            </div>
     


  <hr class="hidden-print">
            <div class="row">
                <div class="col-xs-12" style="margin-left:20%; margin-right:20%">
                    <address class="text-uppercase">
                   <strong>Cliente:</strong> {{$row->name}}<br>
                      <strong>  RIF.: </strong> {{$row->rif}}<br>
                  
                        <strong>Dirección:</strong> {{$row->direccion}}<br>
                       <strong> Teléfono:</strong>  {{$row->telefono}}<br>
                  
</address>

</div>
</div>


<div class="row" style="padding-bottom:5px">
  <div class="col-xs-12">
  <?php $fac= 'FACTURA Nº '; ?>


               

                    </address>
                          
                  
  <?php 
  $dt = new DateTime($row->created_at);
  $df = new DateTime($row->vence);
         ?>
                  <div class="col-xs-8 "> 

  <strong>{{$fac.$row->order_numero}}  <span style="margin-left:10%;"> CONDICIONES DE PAGO:</strong> <span class="text-uppercase">{{$row->opcional}}</span></span> </strong>
                  </div>
                  <div class="col-xs-4">   
    <div class="pull-right">                 
  <strong>Fecha de Emisión: </strong>{{ $dt->format('d-m-Y')}}<br>
  <strong>Fecha de Vencimiento: </strong>{{ $df->format('d-m-Y')}}
       </div>         
                   

</div>
</div>
</div>


     



    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
            
                <div class="panel-body">
  

                  <div class="table-responsive">
                        <table class="table table-condensed" id="table-orderdetail">
                            <thead>
                                <tr class="text-uppercase">
                                  <td><strong>Código</strong></td>
                                      <td><strong>Descripción</strong></td>
                                 
                                    <td class="text-center"><strong>Cant.</strong></td>
                                       <td class="text-right"><strong>Precio</strong></td>
                                 
                                    <td class="text-right"><strong>Total</strong></td>
                                    <td width="10px" class="text-right"><strong>&nbsp;</strong></td>
                                     
                                </tr>
                            </thead>
                            <tbody>
                                <!-- foreach ($order->lineItems as $line) or some such thing here -->
        <?php 

//echo $row->detalle;


        $items = json_decode( $row->detalle);  ?>                       
 
@foreach ($items as $item)
  

 
                                <tr >
                                  <td>{{$item->items_sku}} </td>
                                    <td> {{$item->name}}</td>

                                  
                                    <td class="text-center">{{$item->cantidad}}</td>
                                      <td class="text-right"> {{number_format($item->precio,2,',','.')}}</td>
                                    
                                    <td class="text-right"> <span class="total">{{ number_format($item->total,2,',','.') }}</span></td>
                                   <td class="text-right"></td>
                                </tr>
                          
 
@endforeach


                       
                                <tr>
                                    <td class="thick-line"></td>
                                    <td class="thick-line"></td>
                                    <td class="thick-line"></td>
                                    <td class="thick-line text-right"><strong>TOTAL NETO</strong></td>
                                    <td class="thick-line text-right ">{{$row->moneda}} <span class="sub_total_total99">{{number_format($row->sub_total,2,',','.')}}</span></td>
                                    <td class="thick-line"></td>
                                </tr>




     <tr>
                                    <td class="thick-line"></td>
                                    <td class="thick-line"></td>
                                    <td class="thick-line"></td>
                                    <td class="thick-line text-right"><strong>IVA {{$row->tax}}%</strong></td>
                                    <td class="thick-line text-right ">{{$row->moneda}} 
                   
                                    <span class="total_iva_e">{{number_format($row->tax_total,2,',','.')}}</span></td>
                                    <td class="thick-line"></td>
                                </tr>
                   
                         



                                <tr>
                                <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line text-right"><strong>TOTAL FAC.</strong></td>
                                    <td class="no-line text-right"><strong>{{$row->moneda}}<span id="gran_total99"> {{number_format($row->grand_total,2,',','.')}}
                                     </span></strong></td>

                                     <input type="hidden" id="granTotal" v-model="gran_total" name="">
                                </tr>
                            </tbody>
                        </table>


                    </div>


                </div>

            </div>
         



            
                  
        </div>
    </div>






 

</div> <!--vue fin -->


  





 

        
    </body>
</html>
