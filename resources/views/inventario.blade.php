<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>GeaLight</title>
      

 
        <link rel="stylesheet" href="{{ asset('css/app.css') }}" >
    
 

      
    </head>
<body>
        <div class="container" id="vue">

 
       
        <form action="" method="GET" role="form"  @submit.prevent="guardarOrder">



<div class="hidden-print">

  
     
@include('nav-inv')





</div>
  <div class="panel panel-warning hidden-print" >
      <div class="panel-heading">
          <h3 class="panel-title">Entrada y Salidas de inventario</h3>
      </div>
      <div class="panel-body">
       
<div class="row">
        <div class="col-md-3">
      <label>Documento:</label>  
<select name="documento" v-model="documento" id="input" class="form-control" required="required">
<option value="Entrada">Entrada</option>
<option value="Salida">Salida</option>
 
</select>

        </div>
        <div class="col-md-2">
         <label>Fecha de emisión:</label>
<input type="date" readonly name="fecha_emi" v-model="fecha_emi" id="inputFecha_emi" class="form-control" value="" required="required" title="">

        </div>
        <div class="col-md-2 hidden">
            
      <label>Tax:</label>  
<select name="tax" v-model="tax" id="input" class="form-control" required="required">
<option value="0">0</option>
<option value="5">5</option>
<option value="7">7</option>
<option value="9">9</option>
<option value="10">10</option>
<option value="12">12</option>

</select>

        </div>


                <div class="col-md-2 hidden">
            
      <label>Moneda:</label>  
<select name="moneda" v-model="moneda" id="input" class="form-control" required="required">
<option value="Bs">Bs</option>
<option value="$">$</option>
<option value="€">€</option>
 

</select>

        </div>

       <div class="col-md-3 hidden" >
            
      <label>Config Adicionales:</label>  <br>
      Empresa
        <input type="checkbox" v-model="show_empresa" value="true">
     Cuentas
        <input type="checkbox" v-model="show_ctas" value="true">
    Logo
        <input type="checkbox" v-model="show_logo" value="true">
   
      </div>
        </div>

<br>
    

    <div class="row">
  
  <div class="col-md-12">
      <label>¿Describa el motivo de ajuste del inventario?</label> 

<input type="text" name="detale" required id="inputdetale" class="form-control" placeholder="¿Describa el motivo de ajuste del inventario?"  value="" v-model="descripcion"   title="">

</div>

        <div class="row hidden">

<div class="col-md-3">
    <label>Documento:</label>
    <span class="label label-success" v-if="contactos_id">Buen trabajo <i class="glyphicon glyphicon-check"></i> </span> 
 
<input type="text" name="documento" id="inputdocumento" @keyup.enter.prevent="buscarDoc" class="form-control" value="" v-model="docBuscar" placeholder="Presiona Enter para buscar"    title="">
  

</div>



<div class="col-md-3">

<input type="hidden" v-model="contactos_id" name="contactos_id">
    <label>Dirigido a Nombre:</label>  
<input type="text" name="name" autocomplete="off" id="inputName" @keyup.enter.prevent="buscarContactos" class="form-control" value="" v-model="name" placeholder="Enter para buscar"  title="">
   <p class="help-block ">Palabra clave más "Enter" para buscar.</p>
<ul class="list-group" v-if="panelb">
  <li style="cursor: pointer;" class="list-group-item" v-for="item in contactosBuscados" @click="selectContacto( item.id, item.name, item.direccion, item.telefono, item.documento)">@{{item.id}} - @{{item.name}} - @{{item.documento}}</li>
 
</ul>

</div>
<div class="col-md-3">
      <label>Dirección:</label>  

 <input type="text" name="direc" id="inputDirec" v-model="direc" readonly="true" placeholder="Dirección" class="form-control" value=""  title="">

</div>
<div class="col-md-3">
      <label>Teléfono:</label> 

      <input type="tel" name="tel" id="inputTel" class="form-control" placeholder="Teléfono" readonly="true" value="" v-model="tel" required="required" title="">

</div>

 
</div>
<br>


      </div>
  </div>

 </div>

    <div class="row container">
      <img v-if="show_logo" src="{{ asset('images/GEAEXPRES-LOGO.png') }}" class="pull-right"  alt="">
        <div class="col-xs-12">

            <div class="invoice-title">

            <?php 
$stamp = date("ymdhs");
$rando = mt_rand(0,9); 
$order_number  = str_pad(0, 1, 0 , STR_PAD_RIGHT);
$order_number .= $stamp.$rando;

             ?>
    <span class="hidden" id="order_numero">{{$order_number}}</span>
                <h2>Ajuste de @{{documento}}</h2><h3 class="pull-right"> #{{$order_number}} </h3>
            </div>
           
          
       
        </div>
    </div>
    
 
<div class="panel panel-info  hidden-print" v-if="contactos_id">
  <div class="panel-heading">
    <h3 class="panel-title">Agregar items</h3>
  </div>
  <div class="panel-body">
               <div class="row" >
    
<div class="col-md-3">   
<label>Descripción:</label>  
<input type="text" autocomplete="off" name="" id="input" @keyup.prevent="buscarArticulos" v-model="items" class="form-control" value=""  title="" placeholder="Descripción">
<ul class="list-group" v-if="panelc">
  <li style="cursor: pointer;" class="list-group-item" v-for="item in articulosBuscados" @click="selectArticulos( item.id, item.name, item.precio, item.sku)">@{{item.sku}} - @{{item.name}} - PV@{{item.precio}} - PC@{{item.precio_compra}} - STOCK@{{item.stock}}</li>
 
</ul>
</div>

<input type="hidden" name="items_id" v-model="items_id" >
<input type="hidden" name="items_sku" v-model="items_sku" >

<div class="col-md-2 hidden">
<label>Precio:</label>  
<input type="number" step="0.01"  name="" id="input" v-model="precio" readonly="true" class="form-control" value=""  title="">

</div>
<div class="col-md-2">    
<label>Cantidad:</label> 
<input type="number" name="" id="input" v-model="cantidad" class="form-control" value=""  title="">

</div>
<div class="col-md-2 hidden" >
 <label>Total:</label>    
<input type="text" name="" id="input" readonly="true" v-model="total" class="form-control" value=""  title="">

</div>

<div class="col-md-2">
 <label>Acción:</label><br>    
<button type="button" v-bind:disabled="isButtonDisabled"   class="btn btn-success" @click="agregar"> <i class="glyphicon-plus"></i> Agregar</button>

</div>

 
</div>
  </div>
</div>


    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Resumen @{{documento}}</strong></h3>
                </div>
                <div class="panel-body">
  

                  <div class="table-responsive">
                          


                          <table class="table table-condensed" id="table-orderdetail">
                            <thead>
                                <tr>
                                <td class=""><strong>Id</strong></td>
                                    
                                    <td><strong>Descripción</strong></td>
                                     
                                    <td class="text-center"><strong>Cantidad</strong></td>
                                    <td class="text-right"><strong>Total</strong></td>
                                    <td width="10px" class="text-right"><strong>&nbsp;</strong></td>
                                     
                                </tr>
                            </thead>
                            <tbody>
                                <!-- foreach ($order->lineItems as $line) or some such thing here -->
                                

 
                                <tr v-for="item, index in row">
                                <td>@{{item.items_sku}}</td>
                                    <td>@{{item.name}}</td>

                                  
                                    <td class="text-center">@{{item.cantidad}}</td>
                                    
                                    <td class="text-right">@{{moneda}}<span class="sub_total">@{{item.total}}</span></td>
                                   <td class="text-right"><button v-if="show_control" type="button" @click="removeRow(index)" class="hidden-print btn btn-danger btn-xs"> Eliminar </button></td>
                                </tr>
                          
 


                       
                                <tr>
                                    <td class="thick-line"></td>
                                    <td class="thick-line"></td>
                                    <td class="thick-line text-center"><strong>Subtotal</strong></td>
                                    <td class="thick-line text-right ">@{{moneda}}<span class="sub_total_total">0</span></td>
                                    <td class="thick-line"></td>
                                </tr>
                                <tr v-show="tax!=0">
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line text-center" ><strong>Tax <span id="taxe">@{{tax}}</span>%</strong></td>
                                    <td class="no-line text-right">@{{moneda}}<span id="tax">0</span></td>


                                </tr>
                                <tr>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line text-center"><strong>Total</strong></td>
                                    <td class="no-line text-right"><strong>@{{moneda}}<span id="gran_total"> 
                                     0</span></strong></td>

                                     <input type="hidden" id="granTotal" v-model="gran_total" name="">
                                </tr>
                            </tbody>

                        </table>

                    </div>


                </div>

       


            <div v-if="guardar">
            <button  type="submit" class="btn btn-success pull-right hidden-print"  v-bind:disabled="btn_guardar"><span class="glyphicon glyphicon-save"></span> Guardar</button>
            </div>
 <div v-else="guardar">

<div class="alert alert-success hidden-print">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <strong>@{{documento}} fue guardada con exito! </strong> Precione F5 para crear uno nuevo, o <a target="_blank" href="admin" class="btn btn-info btn-xs">Ir al Admin</a>
</div>

 </div>


         
       

            
     </div>


 </div> 
 

</form>

 

 
 
</div>



  

</div><!--vue fin -->



 


<script type="text/javascript" src="{{ asset('js/app-inventario.js') }}"></script>
        
    </body>
</html>
