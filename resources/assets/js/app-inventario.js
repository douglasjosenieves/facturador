
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
 
var swal = require('sweetalert2');


window.axios = require('axios');

window.axios.defaults.headers.common = {
  //  'X-CSRF-TOKEN': window.Laravel.csrfToken,
    'X-Requested-With': 'XMLHttpRequest'
};

//var Autocomplete = require('vue2-autocomplete-js');
/*swal(
  'GeaLitgh!',
  'demo masinfo:info@tuweblink.com!',
  'success'
)

swal({
  title: 'GeaLitgh',
  html: $('<div>')
    .addClass('some-class')
    .text('Para mas info:info@tuweblink.com!'),
  animation: true,
  customClass: 'animated bounce'
})*/

var vm = new Vue({

  el: "#vue",

  data () {
    return {
      name:'Ajuste de Inentario al GeaLigth',
      contactos_id:'1',
      documento: 'Entrada',
      opcional:'',
      fecha_emi: formattedDate(),
      order_numero:'',
      tax: 0,
      direc:'',
      tel:'',
      show_logo:false,
      show_empresa:false,
      show_control:true,
        show_ctas:false,
     
      row:[],
      obj_row:'',
      items:'',
      precio:0,
       precio_compra:0,
      cantidad:0,
      items_orders_id:'',
      items_orders_doc:'',
      items_id:'',
       items_sku:'',
 

        
        moneda:'Bs',
     
     tipo_reg:'',
      doc_reg:'',
      name_reg:'',
      direc_reg:'',
      tel_reg:'',
      email_reg:'',

contacto_nombre:'',
contacto_telefono:'',
contacto_email:'',
contacto_detalles:'',


      token_reg:'b8VFZxv8sGTkZwKP',
 docBuscar:'',
panelb:true,
panelc:true,
contactosBuscados:[],
articulosBuscados:[],

descripcion:'',
sub_total:'',
tax_total:'',
gran_total:'',

create_art_sku:'',
create_art_name:'',
create_art_precio:'',
create_art_categoria:'',
create_art_descripcion:'',

create_art_tax:'',
create_art_opcional:'',
create_art_opcional1:'',
articulosCategorias:[],
guardar:true,
 
      
    };
  },
       computed: {
                total: function(){
                    return this.precio * this.cantidad;
                },

                isButtonDisabled: function(){
                   if (this.total==0) {
                       return true;
                   
                   }

                   else{

                     return false;
                   }
                },

                btn_guardar: function(){
                  if (this.gran_total==0) {
                       return true;
                   
                   }

                   else{

                     return false;
                   }
                },

            },

            methods: {
              agregar () {

            this.row.push({  items_orders_id:this.order_numero, items_orders_doc:this.documento,  items_id:this.items_id, items_sku:this.items_sku, name:this.items,precio:this.precio, cantidad:this.cantidad, total:this.total});
                this.items_id = "";
                this.items_sku = "";
                this.items = "";
                this.precio = "";
                this.cantidad = "";
                this.items_orders_id="";
                items_orders_doc="";
                //this.total = "";
              },


                removeRow(index){
 
swal({
  title: 'Usted esta seguro?',
  text: "VA a eliminar el items!",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Si, Eliminar!'
}).then(function () {
  vm.row.splice(index,1); // why is this removing only the last row?

  swal(
    'Elimiado!',
    'El items ha sido eliminado con éxito!.',
    'success'
  )


})


      
    },


      getContacto () {
axios.get('api/contactos', {
    params: {
      token: this.token_reg,
    },
  })
  .then(function (response) {
   // console.log(response.data);
 // vm.row = response.data;
  })
  .catch(function (error) {
    console.log(error);
  });
              },

 
      getCategorias () {
axios.get('api/articulos_categorias', {
    params: {
      token: this.token_reg,
    },
  })
  .then(function (response) {
    //console.log('categorias');
   // console.log(response.data);
 vm.articulosCategorias = response.data;
  })
  .catch(function (error) {
    console.log(error);
  });
              },


      


buscarDoc(){
axios.get('api/contactos', {
    params: {
      documento: vm.docBuscar,
      token: this.token_reg,
    },
  })
  .then(function (response) {
    console.log(response.data);
 // vm.row = response.data;

 if(response.data ==''){

swal(
  'Oops...',
  'No encontramos información!',
  'warning'
);

 

vm.name= '';
vm.direc='' ;
vm.tel= '';
vm.contactos_id= '';
vm.docBuscar= '' ;


 }

 else {
vm.name= response.data[0].name;
vm.direc=response.data[0].direccion;
vm.tel=response.data[0].telefono;
vm.contactos_id=response.data[0].id;
vm.docBuscar=response.data[0].documento;

 }
  })
  .catch(function (error) {
    console.log(error);
  });

 // alert(vm.docBuscar)
},

buscarContactos(){
vm.panelb = true;
axios.get('api/contactos/buscar', {
    params: {
      name: vm.name,
      token: this.token_reg,
    },
  })
  .then(function (response) {
    console.log(response.data);
vm.contactosBuscados = response.data;

 
  })
  .catch(function (error) {
    console.log(error);
  });

 // alert(vm.docBuscar)
},






selectContacto(id, name, direccion, telefono, documento){

vm.name= name;
vm.direc=direccion;
vm.tel=telefono;
vm.contactos_id=id;
vm.docBuscar=documento;
vm.panelb = false;

},


buscarArticulos(){
vm.panelb = true;
axios.get('api/articulos/buscar', {
    params: {
      name: vm.items,
      token: this.token_reg,
    },
  })
  .then(function (response) {
    console.log(response.data);
vm.articulosBuscados = response.data;

 
  })
  .catch(function (error) {
    console.log(error);
  });

 // alert(vm.docBuscar)

},


selectArticulos(id, name, precio, sku){

vm.items= name;
vm.precio=precio;
vm.items_id =id;
vm.items_sku =sku;
vm.panelc = false;

},


buscarArticulos(){
vm.panelc = true;
axios.get('api/articulos/buscar', {
    params: {
      name: vm.items,
      token: this.token_reg,
    },
  })
  .then(function (response) {
 //   console.log(response.data);
vm.articulosBuscados = response.data;

 
  })
  .catch(function (error) {
    console.log(error);
  });

 // alert(vm.docBuscar)

},


selectArticulos(id, name, precio, sku){

vm.items= name;
vm.precio=precio;
vm.items_id =id;
vm.items_sku =sku;
vm.panelc = false;

},


  setContactos(){
       
axios.get('api/contactos/set', {
    params: {
      tipo: this.tipo_reg,
      documento: this.doc_reg,
      name: this.name_reg,
      telefono: this.tel_reg,
      email: this.email_reg,
      direccion: this.direc_reg,
      token: this.token_reg,
      opcional: this.opcional,
      contacto_nombre:this.contacto_nombre,
contacto_telefono:this.contacto_telefono,
contacto_email:this.contacto_email,
contacto_detalles:this.contacto_detalles,
    },
  })
  .then(function (response) {
  //  console.log(response.data);

var res = response.data;
  if (res=='1') {
swal(
  'Buen trabajo!',
  'Clic en el botón!',
  'success'
)
vm.tipo_reg='';
vm.doc_reg='';
vm.name_reg='';
vm.tel_reg='';
vm.email_reg='';
vm.direc_reg='';
vm.opcional ='';
vm.contacto_nombre ='';
vm.contacto_telefono ='';
vm.contacto_email ='';
vm.contacto_detalles ='';


}

  })
  .catch(function (error) {
    console.log(error);
  });

    },


 setArticulos(){
       
axios.get('api/articulos/set', {
    params: {
      sku: this.create_art_sku,
      name: this.create_art_name,
      precio: this.create_art_precio,
      precio_compra: this.precio_compra,
      create_art_tax: this.create_art_tax,
       create_art_opcional: this.create_art_opcional,
     create_art_opcional1: this.create_art_opcional1,


      articulos_categorias_id: this.create_art_categoria,
      description: this.create_art_descripcion,
      token: this.token_reg,
    },
  })
  .then(function (response) {
    console.log(response.data);

var res = response.data;
  if (res=='1') {


swal(
  'Buen trabajo!',
  'Clic en el botón!',
  'success'
)
 

vm.create_art_sku='';
vm.create_art_name='';
vm.create_art_precio='';
vm.create_art_categoria='';
vm.create_art_descripcion='';
vm.precio_compra='';
vm.create_art_tax='';
vm.create_art_opcional='';
vm.create_art_opcional1='';
}

  })
  .catch(function (error) {
    console.log(error);
  });

    },


    guardarOrder(){
       
axios.get('api/inventario-set', {
    params: {
     contactos_id: this.contactos_id ,

     descripcion:this.descripcion,
     tax: this.tax ,
     moneda: this.moneda ,
     documento: this.documento ,
     order_numero: this.order_numero ,
     sub_total: this.sub_total,
     tax_total: this.tax_total ,
     grand_total: this.gran_total ,
     detalle: JSON.stringify(this.row) ,
    token: this.token_reg,
      },



  })
  .then(function (response) {
    console.log(response.data);

var res = response.data;
  if (res=='1') {


swal(
  'Buen trabajo!',
  'Clic en el botón!',
  'success'
)
 

vm.guardar=false;


}

  })
  .catch(function (error) {
    console.log(error);
  });

    },


}


          



})


//vm.getCategorias();

//vm.getContacto();

function formattedDate() {
 
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!

var yyyy = today.getFullYear();
if(dd<10){
    dd='0'+dd;
} 
if(mm<10){
    mm='0'+mm;
}
  return yyyy+'-'+mm+'-'+dd;
}



setInterval(function () {
  total=0;
  
/*=========================================================
=            cuenta los totales del los items             =
=========================================================*/
  $('#table-orderdetail tbody .sub_total').each(function() {
              
                   total += parseFloat($(this).text()  );
    
 
                })

/*=====  End of cuenta los totales del los items   ======*/

$('#table-orderdetail tbody .sub_total_total').html(total); 

var subtotal = parseFloat($('.sub_total_total').html());
vm.sub_total = subtotal;
var tax = parseFloat($('#taxe').html());
 


var tax_total = subtotal * tax /100;
$('#tax').html(tax_total);
vm.tax_total = tax_total;

var gran_total = subtotal + tax_total;
$('#gran_total').html(gran_total);
$('#granTotal').val(gran_total);


vm.gran_total = gran_total;
vm.order_numero = parseFloat($('#order_numero').html());

//console.log(subtotal);

},500);



 
 