
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs. 
 */
 
var swal = require('sweetalert2');


window.axios = require('axios');

window.axios.defaults.headers.common = {
  //  'X-CSRF-TOKEN': window.Laravel.csrfToken,
    'X-Requested-With': 'XMLHttpRequest'
};

 
require('./empresa');


var vm = new Vue({

  el: "#vue",

  data () {
    return {

/*=======================================================
=            datos de la empresa para el pdf            =
=======================================================*/ 
empresa:{
name:empresa, 
tel:tel, 
website:website, 
email:email, 
direc:direc, 
rif:rif, 
slogan:slogan
},

ctas_bancarias:ctas_bancarias,
banco_responsable:banco_responsable,
/*=====  End of datos de la empresa para el pdf  ======*/



/*======================================
=            ecabezados var            =
======================================*/ 
fvid:'',
name:'',
contactos_id:'',
documento: 'Compra',
fecha_emi: formattedDate(),
vence:formattedDate(),
order_numero:'',
tax: 12,
direc:'',
tel:'',
show_logo:false,
show_empresa:false,
show_control:true,
show_ctas:false,  
opcional_enc:'', /*----------  Condiciones  ----------*/
opcional1_enc:'', /*----------  umero de control  ----------*/
vendedor:[],
vendedors:'',
moneda:'Bs',
/*=====  End of ecabezados var  ======*/



/*================================
=            reglones            =
================================*/
row:[], /*----------  El array de los item que estan ya agregado  ----------*/
items:'',
precio:0,
precio_compra:0,
cantidad:0,
tax_i: 0,
items_orders_id:'', /*----------  Id del Encabezado  ----------*/
items_id:'',
items_orders_doc:'', /*----------  si es Factura, Orden, Cotizacion  ----------*/
items_sku:'',
items_tax:'',
/*=====  End of reglones  ======*/

 
   
 

   
/*==========================================
=            Variables de Apoyo            =
==========================================*/
docBuscar:'',   /*----------  Rif o Nit buscado  ----------*/
panelb:true,   /*----------  Lista de Cotactos por nombre  ----------*/
panelc:true, /*----------  Lista de reglones por nombre  ----------*/
guardar:true, /*----------  Btn para disabled  ----------*/
contactosBuscados:[],
articulosBuscados:[],
token_reg:'b8VFZxv8sGTkZwKP',
guardarY:'',
/*=====  End of Variables de Apoyo  ======*/
              


 
      
};




  },
       


       computed: {

totalSub: function () {
var total = 0;
this.row.forEach(function (row) {

total += row.precio * row.cantidad;

});
return total;
},


totaliva: function () {
var total = 0;
total = this.totalSub * this.tax / 100
return total;
},

totalgrand: function () {
var total = 0;
total = this.totalSub + this.totaliva 
return total;
},


total: function(){
return (this.precio * this.cantidad) ;
},

   

isButtonDisabled: function(){
if (this.total==0) {
return true;
}

else{
return false;
}
},

btn_guardar: function(){
if (this.totalgrand==0) {
return true;
}

else{
return false;
}
},




                  }, /*----------  Fin Computed  ----------*/









            methods: {

verificaStock(){


},


verificaId(){
this.getId();
},



getId () {
//PARA OBTENER EL ID
var url = 'api/orders-set-compras/getid';

axios.get(url, {
params: {
token: this.token_reg,
},
})
.then(function (response) {
//console.log('categorias');
//console.log(response.data);
vm.fvid = response.data;
})
.catch(function (error) {
console.log(error);
});
},



  getVendedor () {
axios.get('api/vendedor/buscar', {
    params: {
      token: this.token_reg,
    },
  })
  .then(function (response) {
    //console.log('categorias');
  // console.log(response.data);
 vm.vendedor = response.data;
  })
  .catch(function (error) {
    console.log(error);
  });
              },

 

              agregar () {
this.row.push({ 
items_orders_id:this.order_numero, 
items_orders_doc:this.documento,  
items_id:this.items_id, items_sku:this.items_sku, 
items_tax:this.tax_i, 
name:this.items, 
precio:this.precio,
cantidad:this.cantidad,
total:this.total



              });


this.items_id = "";
this.items_sku = "";
this.tax_i = "";
this.items = "";
this.precio = "";
precio_compra="";
this.cantidad = "";
this.items_orders_id="";
items_orders_doc="";


        
              },


                removeRow(index){
 
swal({
  title: 'Usted esta seguro?',
  text: "VA a eliminar el items!",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Si, Eliminar!'
}).then(function () {
  vm.row.splice(index,1); // why is this removing only the last row?

  swal(
    'Elimiado!',
    'El items ha sido eliminado con éxito!.',
    'success'
  )


})


      
    },


      getContacto () {
axios.get('api/contactos', {
    params: {
      token: this.token_reg,
    },
  })
  .then(function (response) {
   // console.log(response.data);
 // vm.row = response.data;
  })
  .catch(function (error) {
    console.log(error);
  });
              },

 




 
     

buscarDoc(){
axios.get('api/contactos', {
    params: {
      documento: vm.docBuscar,
      token: this.token_reg,
    },
  })
  .then(function (response) {
   console.log(response.data);
 // vm.row = response.data;

 if(response.data ==''){

//swal(
  //'Oops...',
  //'No encontramos información!',
  //'warning'
//);

 

vm.name= '';
vm.direc='' ;
vm.tel= '';
vm.contactos_id= '';
//vm.docBuscar= '' ;


 }

 else {
vm.name= response.data[0].name;
vm.direc=response.data[0].direccion;
vm.tel=response.data[0].telefono;
vm.contactos_id=response.data[0].id;
vm.docBuscar=response.data[0].documento;

 }
  })
  .catch(function (error) {
    console.log(error);
  });

 // alert(vm.docBuscar)
},

buscarContactos(){
vm.panelb = true;
axios.get('api/contactos/buscar', {
      params: {
      name: vm.name,
      token: this.token_reg,
    },
  })
  .then(function (response) {
    console.log(response.data);
vm.contactosBuscados = response.data;

 
  })
  .catch(function (error) {
    console.log(error);
  });

 // alert(vm.docBuscar)
},






selectContacto(id, name, direccion, telefono, documento){

vm.name= name;
vm.direc=direccion;
vm.tel=telefono;
vm.contactos_id=id;
vm.docBuscar=documento;
vm.panelb = false;

},


 





buscarArticulos(){
vm.panelc = true;
axios.get('api/articulos/buscar', {
      params: {
      name: vm.items,
      token: this.token_reg,
      },
      })
      .then(function (response) {
      //console.log(response.data);
      vm.articulosBuscados = response.data;


      })
      .catch(function (error) {
      console.log(error);
      });

 // alert(vm.docBuscar)

},


selectArticulos(id, name, precio, sku, tax,precio_compra){

vm.items= name;
vm.precio=precio_compra;
vm.items_id =id;
vm.items_sku =sku;
vm.panelc = false;
vm.tax_i= tax;

},


 
guardarComo(como){

this.guardarY = como;

},



    guardarOrder(){
vm.getId();



var url = 'api/orders-set-compras';
axios.get(url, {
        params: {
        contactos_id: this.contactos_id ,
        tax: this.tax ,
        vendedor_id: this.vendedors,
        opcional: this.opcional_enc,
        opcional1: this.opcional1_enc,
        vence: this.vence,
        moneda: this.moneda ,
        documento: this.documento ,
        order_numero: '0000'+this.fvid ,
        sub_total: this.totalSub,
        tax_total: this.totaliva ,
        grand_total: this.totalgrand ,
        detalle: JSON.stringify(this.row) ,
        token: this.token_reg,
      },



  })
  .then(function (response) {
    console.log(response.data);

var res = response.data;
  if (res=='1') {



vm.guardar=false;


if (vm.guardarY == 'guardarynuevo') {

location.reload();
}

else {
location.href="admin/orders_compra/detail/"+vm.fvid;
}


}

alert('Compra guardada...!')

  })
  .catch(function (error) {
    console.log(error);
  });

    },




}    /*----------  Fin Method  ----------*/


          



})


 
 
vm.getId();
  vm.getVendedor();
function formattedDate() {
 
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!

var yyyy = today.getFullYear();
if(dd<10){
    dd='0'+dd;
} 
if(mm<10){
    mm='0'+mm;
}
  return yyyy+'-'+mm+'-'+dd;
}











 
 