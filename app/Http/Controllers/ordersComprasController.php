<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class ordersComprasController extends Controller
{


 public function setOrden(Request $request)
 {

$token = $request->input('token', '0');
$contactos_id = htmlspecialchars($request->input('contactos_id', '0'));
$tax = htmlspecialchars($request->input('tax', '0'));
$moneda = htmlspecialchars($request->input('moneda', '0'));
$documento = htmlspecialchars($request->input('documento', '0'));
$order_numero = htmlspecialchars($request->input('order_numero', '0'));
$sub_total = htmlspecialchars($request->input('sub_total', '0'));
$tax_total = htmlspecialchars($request->input('tax_total', '0'));
$grand_total = htmlspecialchars($request->input('grand_total', '0'));
$saldo = htmlspecialchars($request->input('grand_total', '0'));
$vence = htmlspecialchars($request->input('vence', '0'));

$vendedor_id = htmlspecialchars($request->input('vendedor_id', NULL));
$opcional = htmlspecialchars($request->input('opcional', NULL));
$opcional1 = htmlspecialchars($request->input('opcional1', NULL));
$tipo_reg = htmlspecialchars($request->input('tipo_reg', '0'));


$detalle =  $request->input('detalle', '0');
 

if ($token=='b8VFZxv8sGTkZwKP') {


 

 
$items = json_decode( $detalle,true);




/*====================================
=             MOVIMIENTOS            =
====================================*/

foreach ($items as $key => $value) {
	$items_mov[] =  array(
'items_orders_id' => $order_numero,
'items_orders_doc' => $value['items_orders_doc'],
'items_id' => $value['items_id'],
'items_sku' => $value['items_sku'],
'name' => $value['name'],
'precio' => $value['precio'],
'cantidad' => $value['cantidad'],
'total' => $value['total']
 ); 
}



	$movimiento = $items_mov;
	DB::table('movimientos_detail')->insert($movimiento);


 
 

$movimiento_update =  DB::table('movimientos_detail')->select('items_sku', DB::raw('SUM(cantidad) as total_cantidad'))->whereNull('deleted_at')

 
                ->groupBy('movimientos_detail.items_sku')->get();


foreach ($movimiento_update as $key => $value) {

DB::table('articulos')
            ->where('sku', $value->items_sku)
            ->update(['stock' => $value->total_cantidad]);
//echo $value->items_sku.' ';
 // echo $value->total_cantidad.'<br>';

}



//print_r($items_mov);


/*=====  End of  MOVIMIENTOS  ======*/



//print_r($items);


DB::table('orders_detail_compra')->insert($items);



//echo $orders_detail;
 // print_r($request->all());
 
// `gealite`.`orders`
$orders = array(
  array('created_at' => now(),'updated_at' => NULL,'deleted_at' => NULL,'contactos_id' => $contactos_id,  'tax' => $tax ,'moneda' => $moneda ,'vence' => $vence,'documento' => $documento,'vendedor_id' => $vendedor_id, 'opcional' => $opcional, 'opcional1' => $opcional1, 'order_numero' => $order_numero,'sub_total' => $sub_total,'tax_total' => $tax_total,'detalle' => $detalle,'grand_total' => $grand_total, 'saldo' => $saldo,'enviado' => '0')
);

$res =  DB::table('orders_compra')->insert($orders);
echo $res;


 


} else {
echo 'Toke invalido';
}


 }




 public function  getId($value='')
 {
 	$orders_fv = DB::table('orders_compra')->orderBy('id', 'desc')->first();

 	 return $orders_fv->id + 1;
 }

 }
