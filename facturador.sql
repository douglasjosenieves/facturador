-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 22-01-2018 a las 21:39:00
-- Versión del servidor: 10.1.28-MariaDB
-- Versión de PHP: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `facturador`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulos`
--

CREATE TABLE `articulos` (
  `id` int(10) UNSIGNED NOT NULL,
  `sku` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `precio` double DEFAULT NULL,
  `precio_compra` double DEFAULT NULL,
  `stock` double NOT NULL DEFAULT '0',
  `tax` double NOT NULL DEFAULT '0',
  `unidad_de_medida` text COLLATE utf8_unicode_ci,
  `articulos_categorias_id` int(11) DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `opcional` text COLLATE utf8_unicode_ci,
  `opcional1` text COLLATE utf8_unicode_ci,
  `opcional2` text COLLATE utf8_unicode_ci,
  `opcional3` text COLLATE utf8_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `articulos`
--

INSERT INTO `articulos` (`id`, `sku`, `name`, `description`, `precio`, `precio_compra`, `stock`, `tax`, `unidad_de_medida`, `articulos_categorias_id`, `photo`, `photo1`, `opcional`, `opcional1`, `opcional2`, `opcional3`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '171354949542', 'Computadora', 'Et molestiae debitis incididunt fugiat ea quia architecto debitis similique dolor doloremque sequi magnam nemo', 45, 5, -1, 12, NULL, 1, NULL, NULL, 'Non saepe obcaecati autem officiis cum mollitia reprehenderit irure nulla tempora magnam', 'Sed ut quis sunt necessitatibus temporibus itaque dicta minima nihil iure quas', NULL, NULL, NULL, '2017-10-30 18:11:49', NULL),
(2, '315123959682', 'Libby Riggs', 'Ipsam perferendis molestiae amet esse et id elit voluptatem Esse sint fuga Sit eius pariatur Aut omnis ea', 53, 42, 0, 45, NULL, 1, NULL, NULL, 'Nulla architecto lorem dolore maiores similique nulla provident molestiae minim nihil', 'Vero quo ad saepe dolorem mollitia et iste nesciunt', NULL, NULL, NULL, '2017-11-04 16:40:16', NULL),
(3, '98874654', 'Dominio con extensión .com', 'Dominio con extensión .com', 850000, 572500, 0, 12, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-06 19:43:21', NULL),
(4, '423423', 'Hosting Ilimitado Periodicidad trimestral', 'Hosting Ilimitado Periodicidad trimestral', 450000, 449999, 0, 12, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-06 19:45:34', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulos_categorias`
--

CREATE TABLE `articulos_categorias` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `articulos_categorias`
--

INSERT INTO `articulos_categorias` (`id`, `name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'CatDemo', NULL, '2017-10-30 18:09:51', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_apicustom`
--

CREATE TABLE `cms_apicustom` (
  `id` int(10) UNSIGNED NOT NULL,
  `permalink` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tabel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `aksi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kolom` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `orderby` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sub_query_1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sql_where` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parameter` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `method_type` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parameters` longtext COLLATE utf8_unicode_ci,
  `responses` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_apikey`
--

CREATE TABLE `cms_apikey` (
  `id` int(10) UNSIGNED NOT NULL,
  `screetkey` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hit` int(11) DEFAULT NULL,
  `status` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_dashboard`
--

CREATE TABLE `cms_dashboard` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_email_queues`
--

CREATE TABLE `cms_email_queues` (
  `id` int(10) UNSIGNED NOT NULL,
  `send_at` datetime DEFAULT NULL,
  `email_recipient` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_from_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_from_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_cc_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_content` text COLLATE utf8_unicode_ci,
  `email_attachments` text COLLATE utf8_unicode_ci,
  `is_sent` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_email_templates`
--

CREATE TABLE `cms_email_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `from_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `from_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cc_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_logs`
--

CREATE TABLE `cms_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `ipaddress` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `useragent` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8_unicode_ci,
  `id_cms_users` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_menus`
--

CREATE TABLE `cms_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'url',
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_dashboard` tinyint(1) NOT NULL DEFAULT '0',
  `id_cms_privileges` int(11) DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `cms_menus`
--

INSERT INTO `cms_menus` (`id`, `name`, `type`, `path`, `color`, `icon`, `parent_id`, `is_active`, `is_dashboard`, `id_cms_privileges`, `sorting`, `created_at`, `updated_at`) VALUES
(1, 'Contactos', 'Route', 'AdminContactosControllerGetIndex', 'normal', 'fa fa-users', 25, 1, 0, 1, 1, '2017-10-08 04:40:38', '2017-10-10 22:07:16'),
(2, 'Artículos Categorías', 'Route', 'AdminArticulosCategoriasControllerGetIndex', 'normal', 'fa fa-tags', 14, 1, 0, 1, 2, '2017-10-10 03:24:21', '2017-10-10 22:07:53'),
(3, 'Artículos', 'Route', 'AdminArticulosControllerGetIndex', 'normal', 'fa fa-cubes', 14, 1, 0, 1, 1, '2017-10-10 03:25:27', '2017-10-10 22:07:38'),
(4, 'Ordenes', 'Route', 'AdminOrdersControllerGetIndex', 'normal', 'fa fa-opencart', 0, 1, 0, 1, 7, '2017-10-10 08:18:24', '2017-10-10 22:08:18'),
(5, 'GeaLigth', 'Route', 'index', 'light-blue', 'fa fa-home', 0, 1, 0, 1, 2, '2017-10-10 10:23:10', '2017-10-27 10:12:33'),
(6, 'Inventario', 'Route', 'AdminInventarioControllerGetIndex', NULL, 'fa fa-cube', 13, 1, 0, 1, 1, '2017-10-14 18:50:18', NULL),
(8, 'Movimientos', 'Route', 'AdminMovimientosDetail18ControllerGetIndex', 'normal', 'fa fa-eye', 13, 1, 0, 1, 2, '2017-10-14 19:30:37', '2017-10-16 21:33:32'),
(9, 'Caja y Bancos', 'Route', 'AdminCuentasCategoriasControllerGetIndex', 'normal', 'fa fa-plus', 12, 1, 0, 1, 1, '2017-10-16 18:00:18', '2017-10-16 21:28:05'),
(10, 'Movimientos', 'Route', 'AdminMovimientosCuentasDetailControllerGetIndex', 'normal', 'fa fa-eye', 12, 1, 0, 1, 3, '2017-10-16 18:04:01', '2017-10-16 21:33:48'),
(11, 'Ver Documentos', 'Route', 'AdminCuentasControllerGetIndex', 'normal', 'fa fa-bank', 12, 1, 0, 1, 2, '2017-10-16 19:26:47', '2017-10-16 21:29:18'),
(12, 'Cobros Pagos Ent Sal', 'URL', '#', 'normal', 'fa fa-dollar', 0, 1, 0, 1, 9, '2017-10-16 21:26:23', '2017-10-16 21:36:44'),
(13, 'Inventarios', 'URL', '#', 'normal', 'fa fa-cube', 0, 1, 0, 1, 8, '2017-10-16 21:32:42', NULL),
(14, 'Articulos', 'URL', '#', 'normal', 'fa fa-cubes', 0, 1, 0, 1, 5, '2017-10-16 21:38:33', NULL),
(15, 'Procesos', 'URL', '#', 'normal', 'fa fa-th-list', 0, 1, 0, 1, 4, '2017-10-27 15:55:24', NULL),
(16, 'Facturas', 'Route', 'index', 'normal', 'fa fa-bookmark', 15, 1, 0, 1, 1, '2017-10-27 15:57:19', NULL),
(17, 'Inventarios', 'Route', 'inventarios', 'normal', 'fa fa-cubes', 15, 1, 0, 1, 2, '2017-10-27 15:58:28', NULL),
(18, 'Cuentas', 'Route', 'cuentas', 'normal', 'fa fa-money', 15, 1, 0, 1, 3, '2017-10-27 15:59:13', NULL),
(19, 'Dashboard', 'Statistic', 'statistic_builder/show/dashboard', 'normal', 'fa fa-dashboard', 0, 1, 1, 1, 1, '2017-10-27 16:10:25', NULL),
(23, 'Clientes', 'Route', 'AdminClientesControllerGetIndex', NULL, 'fa fa-users', 25, 1, 0, 1, 2, '2017-10-30 03:08:25', NULL),
(24, 'Proveedores', 'Route', 'AdminProveedoresControllerGetIndex', NULL, 'fa fa-users', 25, 1, 0, 1, 3, '2017-10-30 03:11:08', NULL),
(25, 'Contactos', 'URL', '#', 'normal', 'fa fa-users', 0, 1, 0, 1, 3, '2017-10-30 03:12:43', NULL),
(26, 'Vendedor', 'Route', 'AdminVendedorControllerGetIndex', 'normal', 'fa fa-suitcase', 0, 1, 0, 1, 10, '2017-10-30 15:09:48', '2017-10-30 18:03:25'),
(27, 'Facturas', 'Route', 'AdminFacturasControllerGetIndex', 'normal', 'fa fa-shopping-cart', 0, 1, 0, 1, 6, '2017-10-30 17:54:24', '2017-10-30 18:02:49');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_menus_privileges`
--

CREATE TABLE `cms_menus_privileges` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_menus` int(11) DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `cms_menus_privileges`
--

INSERT INTO `cms_menus_privileges` (`id`, `id_cms_menus`, `id_cms_privileges`) VALUES
(15, 1, 2),
(16, 1, 1),
(17, 3, 2),
(18, 3, 1),
(19, 2, 2),
(20, 2, 1),
(21, 4, 2),
(22, 4, 1),
(25, 6, 2),
(26, 6, 1),
(27, 6, 1),
(28, 7, 1),
(35, 9, 1),
(36, 11, 2),
(37, 11, 1),
(40, 13, 2),
(41, 13, 1),
(42, 8, 1),
(43, 10, 1),
(44, 12, 2),
(45, 12, 1),
(46, 14, 2),
(47, 14, 1),
(48, 5, 2),
(49, 5, 1),
(50, 15, 2),
(51, 15, 1),
(52, 16, 2),
(53, 16, 1),
(54, 17, 2),
(55, 17, 1),
(56, 18, 2),
(57, 18, 1),
(58, 19, 2),
(59, 19, 1),
(64, 22, 2),
(65, 22, 1),
(72, 21, 2),
(73, 21, 1),
(74, 20, 2),
(75, 20, 1),
(76, 23, 1),
(77, 24, 1),
(78, 25, 2),
(79, 25, 1),
(81, 27, 2),
(82, 27, 1),
(83, 26, 2),
(84, 26, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_moduls`
--

CREATE TABLE `cms_moduls` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `table_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `cms_moduls`
--

INSERT INTO `cms_moduls` (`id`, `name`, `icon`, `path`, `table_name`, `controller`, `is_protected`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Notificaciones', 'fa fa-cog', 'notifications', 'cms_notifications', 'NotificationsController', 1, 1, '2017-10-08 00:10:06', NULL, NULL),
(2, 'Privilegios', 'fa fa-cog', 'privileges', 'cms_privileges', 'PrivilegesController', 1, 1, '2017-10-08 00:10:06', NULL, NULL),
(3, 'Privilegios & Roles', 'fa fa-cog', 'privileges_roles', 'cms_privileges_roles', 'PrivilegesRolesController', 1, 1, '2017-10-08 00:10:06', NULL, NULL),
(4, 'Gestión de usuarios', 'fa fa-users', 'users', 'cms_users', 'AdminCmsUsersController', 0, 1, '2017-10-08 00:10:06', NULL, NULL),
(5, 'Ajustes', 'fa fa-cog', 'settings', 'cms_settings', 'SettingsController', 1, 1, '2017-10-08 00:10:06', NULL, NULL),
(6, 'Generador de Módulos', 'fa fa-database', 'module_generator', 'cms_moduls', 'ModulsController', 1, 1, '2017-10-08 00:10:06', NULL, NULL),
(7, 'Gestión de Menús', 'fa fa-bars', 'menu_management', 'cms_menus', 'MenusController', 1, 1, '2017-10-08 00:10:06', NULL, NULL),
(8, 'Plantillas de Correo', 'fa fa-envelope-o', 'email_templates', 'cms_email_templates', 'EmailTemplatesController', 1, 1, '2017-10-08 00:10:06', NULL, NULL),
(9, 'Generador de Estadísticas', 'fa fa-dashboard', 'statistic_builder', 'cms_statistics', 'StatisticBuilderController', 1, 1, '2017-10-08 00:10:06', NULL, NULL),
(10, 'Generador de API', 'fa fa-cloud-download', 'api_generator', '', 'ApiCustomController', 1, 1, '2017-10-08 00:10:06', NULL, NULL),
(11, 'Log de Accesos (Usuarios)', 'fa fa-flag-o', 'logs', 'cms_logs', 'LogsController', 1, 1, '2017-10-08 00:10:06', NULL, NULL),
(12, 'Contactos', 'fa fa-users', 'contactos', 'contactos', 'AdminContactosController', 0, 0, '2017-10-08 00:10:37', NULL, NULL),
(13, 'Artículos Categorías', 'fa fa-tag', 'articulos_categorias', 'articulos_categorias', 'AdminArticulosCategoriasController', 0, 0, '2017-10-09 22:54:21', NULL, NULL),
(14, 'Artículos', 'fa fa-cubes', 'articulos', 'articulos', 'AdminArticulosController', 0, 0, '2017-10-09 22:55:27', NULL, NULL),
(15, 'Ordenes', 'fa fa-opencart', 'orders', 'orders', 'AdminOrdersController', 0, 0, '2017-10-10 03:48:23', NULL, NULL),
(16, 'Inventario', 'fa fa-cube', 'inventario', 'inventario', 'AdminInventarioController', 0, 0, '2017-10-14 18:50:17', NULL, NULL),
(17, 'Movimientos de inventario', 'fa fa-arrow-circle-o-right', 'movimientos_detail', 'movimientos_detail', 'AdminMovimientosDetailController', 0, 0, '2017-10-14 19:15:38', NULL, '2017-10-14 19:30:07'),
(18, 'Movimientos', 'fa fa-repeat', 'movimientos_detail18', 'movimientos_detail', 'AdminMovimientosDetail18Controller', 0, 0, '2017-10-14 19:30:37', NULL, NULL),
(19, 'Caja y Bancos', 'fa fa-bank', 'cuentas_categorias', 'cuentas_categorias', 'AdminCuentasCategoriasController', 0, 0, '2017-10-16 18:00:17', NULL, NULL),
(20, 'Movimientos C&B', 'fa fa-refresh', 'movimientos_cuentas_detail', 'movimientos_cuentas_detail', 'AdminMovimientosCuentasDetailController', 0, 0, '2017-10-16 18:04:00', NULL, NULL),
(21, 'Cuentas', 'fa fa-bank', 'cuentas', 'cuentas', 'AdminCuentasController', 0, 0, '2017-10-16 19:26:46', NULL, NULL),
(22, 'Clientes', 'fa fa-users', 'clientes', 'contactos', 'AdminClientesController', 0, 0, '2017-10-30 03:08:25', NULL, NULL),
(23, 'Proveedores', 'fa fa-users', 'proveedores', 'contactos', 'AdminProveedoresController', 0, 0, '2017-10-30 03:11:07', NULL, NULL),
(24, 'Vendedor', 'fa fa-suitcase', 'vendedor', 'vendedor', 'AdminVendedorController', 0, 0, '2017-10-30 15:09:45', NULL, NULL),
(25, 'Facturas', 'fa fa-shopping-cart', 'facturas', 'orders_fv', 'AdminFacturasController', 0, 0, '2017-10-30 17:54:23', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_notifications`
--

CREATE TABLE `cms_notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_users` int(11) DEFAULT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_privileges`
--

CREATE TABLE `cms_privileges` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_superadmin` tinyint(1) DEFAULT NULL,
  `theme_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `cms_privileges`
--

INSERT INTO `cms_privileges` (`id`, `name`, `is_superadmin`, `theme_color`, `created_at`, `updated_at`) VALUES
(1, 'Super Administrator', 1, 'skin-blue', '2017-10-10 08:33:30', NULL),
(2, 'Demo', 0, 'skin-blue', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_privileges_roles`
--

CREATE TABLE `cms_privileges_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_visible` tinyint(1) DEFAULT NULL,
  `is_create` tinyint(1) DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `is_edit` tinyint(1) DEFAULT NULL,
  `is_delete` tinyint(1) DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `id_cms_moduls` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `cms_privileges_roles`
--

INSERT INTO `cms_privileges_roles` (`id`, `is_visible`, `is_create`, `is_read`, `is_edit`, `is_delete`, `id_cms_privileges`, `id_cms_moduls`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 0, 0, 0, 1, 1, '2017-10-08 04:40:06', NULL),
(2, 1, 1, 1, 1, 1, 1, 2, '2017-10-08 04:40:06', NULL),
(3, 0, 1, 1, 1, 1, 1, 3, '2017-10-08 04:40:06', NULL),
(4, 1, 1, 1, 1, 1, 1, 4, '2017-10-08 04:40:06', NULL),
(5, 1, 1, 1, 1, 1, 1, 5, '2017-10-08 04:40:06', NULL),
(6, 1, 1, 1, 1, 1, 1, 6, '2017-10-08 04:40:06', NULL),
(7, 1, 1, 1, 1, 1, 1, 7, '2017-10-08 04:40:06', NULL),
(8, 1, 1, 1, 1, 1, 1, 8, '2017-10-08 04:40:06', NULL),
(9, 1, 1, 1, 1, 1, 1, 9, '2017-10-08 04:40:06', NULL),
(10, 1, 1, 1, 1, 1, 1, 10, '2017-10-08 04:40:06', NULL),
(11, 1, 0, 1, 0, 1, 1, 11, '2017-10-08 04:40:06', NULL),
(12, 1, 1, 1, 1, 1, 1, 12, NULL, NULL),
(13, 1, 1, 1, 1, 1, 1, 13, NULL, NULL),
(14, 1, 1, 1, 1, 1, 1, 14, NULL, NULL),
(15, 1, 1, 1, 1, 1, 1, 15, NULL, NULL),
(16, 1, 1, 1, 1, 0, 2, 14, NULL, NULL),
(17, 1, 1, 1, 1, 0, 2, 13, NULL, NULL),
(18, 1, 1, 1, 1, 0, 2, 12, NULL, NULL),
(19, 1, 0, 1, 0, 0, 2, 4, NULL, NULL),
(20, 1, 0, 1, 0, 0, 2, 15, NULL, NULL),
(21, 1, 0, 1, 0, 0, 2, 16, NULL, NULL),
(22, 1, 0, 1, 0, 0, 2, 18, NULL, NULL),
(23, 1, 0, 1, 0, 0, 2, 17, NULL, NULL),
(24, 1, 1, 1, 1, 1, 1, 19, NULL, NULL),
(25, 1, 1, 1, 1, 1, 1, 20, NULL, NULL),
(26, 1, 1, 1, 1, 1, 1, 21, NULL, NULL),
(27, 1, 1, 1, 1, 0, 2, 19, NULL, NULL),
(28, 1, 0, 1, 0, 0, 2, 21, NULL, NULL),
(29, 1, 0, 1, 0, 0, 2, 20, NULL, NULL),
(30, 1, 1, 1, 1, 1, 1, 22, NULL, NULL),
(31, 1, 1, 1, 1, 1, 1, 23, NULL, NULL),
(32, 1, 1, 1, 1, 0, 2, 22, NULL, NULL),
(33, 1, 1, 1, 1, 0, 2, 23, NULL, NULL),
(34, 1, 1, 1, 1, 1, 1, 24, NULL, NULL),
(35, 1, 1, 1, 1, 1, 1, 25, NULL, NULL),
(36, 1, 0, 1, 0, 0, 2, 25, NULL, NULL),
(37, 1, 1, 1, 1, 0, 2, 24, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_settings`
--

CREATE TABLE `cms_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `content_input_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dataenum` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `helper` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `group_setting` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `cms_settings`
--

INSERT INTO `cms_settings` (`id`, `name`, `content`, `content_input_type`, `dataenum`, `helper`, `created_at`, `updated_at`, `group_setting`, `label`) VALUES
(1, 'login_background_color', '', 'text', NULL, 'Input hexacode', '2017-10-07 23:22:58', NULL, 'Login Register Style', 'Login Background Color'),
(2, 'login_font_color', '', 'text', NULL, 'Input hexacode', '2017-10-07 23:22:58', NULL, 'Login Register Style', 'Login Font Color'),
(3, 'login_background_image', 'uploads/2017-10/d8e46983563a8927b1a6e7f16646aa95.jpg', 'upload_image', NULL, NULL, '2017-10-07 23:22:58', NULL, 'Login Register Style', 'Login Background Image'),
(4, 'email_sender', 'support@crudbooster.com', 'text', NULL, NULL, '2017-10-07 23:22:58', NULL, 'Email Setting', 'Email Sender'),
(5, 'smtp_driver', 'mail', 'select', 'smtp,mail,sendmail', NULL, '2017-10-07 23:22:58', NULL, 'Email Setting', 'Mail Driver'),
(6, 'smtp_host', '', 'text', NULL, NULL, '2017-10-07 23:22:58', NULL, 'Email Setting', 'SMTP Host'),
(7, 'smtp_port', '25', 'text', NULL, 'default 25', '2017-10-07 23:22:58', NULL, 'Email Setting', 'SMTP Port'),
(8, 'smtp_username', '', 'text', NULL, NULL, '2017-10-07 23:22:58', NULL, 'Email Setting', 'SMTP Username'),
(9, 'smtp_password', '', 'text', NULL, NULL, '2017-10-07 23:22:58', NULL, 'Email Setting', 'SMTP Password'),
(10, 'appname', 'Gea-Light', 'text', NULL, NULL, '2017-10-07 23:22:58', NULL, 'Application Setting', 'Application Name'),
(11, 'default_paper_size', 'Legal', 'text', NULL, 'Paper size, ex : A4, Legal, etc', '2017-10-07 23:22:58', NULL, 'Application Setting', 'Default Paper Print Size'),
(12, 'logo', 'uploads/2017-10/bacb435e092f597895651094daa56bc4.png', 'upload_image', NULL, NULL, '2017-10-07 23:22:58', NULL, 'Application Setting', 'Logo'),
(13, 'favicon', 'uploads/2017-10/5b9820ab4fc181a852824cd782efe8a1.png', 'upload_image', NULL, NULL, '2017-10-07 23:22:58', NULL, 'Application Setting', 'Favicon'),
(14, 'api_debug_mode', 'true', 'select', 'true,false', NULL, '2017-10-07 23:22:58', NULL, 'Application Setting', 'API Debug Mode'),
(15, 'google_api_key', '', 'text', NULL, NULL, '2017-10-07 23:22:58', NULL, 'Application Setting', 'Google API Key'),
(16, 'google_fcm_key', '', 'text', NULL, NULL, '2017-10-07 23:22:58', NULL, 'Application Setting', 'Google FCM Key');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_statistics`
--

CREATE TABLE `cms_statistics` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `cms_statistics`
--

INSERT INTO `cms_statistics` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Dashboard', 'dashboard', '2017-03-17 23:53:45', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_statistic_components`
--

CREATE TABLE `cms_statistic_components` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_statistics` int(11) DEFAULT NULL,
  `componentID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `component_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `area_name` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `config` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `cms_statistic_components`
--

INSERT INTO `cms_statistic_components` (`id`, `id_cms_statistics`, `componentID`, `component_name`, `area_name`, `sorting`, `name`, `config`, `created_at`, `updated_at`) VALUES
(1, 1, '17620f3b0259d8bb0527a828a46ce27b', 'smallbox', 'area1', 0, NULL, '{\"name\":\"Total Factura de Venta\",\"icon\":\"ion-bag\",\"color\":\"bg-green\",\"link\":\"#\",\"sql\":\"select count(id) from `orders_fv` where documento=\'Factura de Venta\' and deleted_at is null\"}', '2017-03-17 23:53:55', NULL),
(2, 1, 'dc9bf1879b58e69402c7c906ba4414b1', 'smallbox', 'area2', 0, NULL, '{\"name\":\"Total Factura de Venta Cantidad\",\"icon\":\"ion-cash\",\"color\":\"bg-green\",\"link\":\"#\",\"sql\":\"select IFNULL(sum(grand_total),0) from `orders_fv` where documento=\'Factura de Venta\' and deleted_at is null\"}', '2017-03-17 23:55:54', NULL),
(3, 1, 'ef86ce0883e86bdec1f49ead38443305', 'smallbox', 'area3', 0, NULL, '{\"name\":\"Total Factura de Venta Actuales Cantidad\",\"icon\":\"ion-cash\",\"color\":\"bg-red\",\"link\":\"#\",\"sql\":\"select IFNULL(SUM(grand_total),0) from `orders_fv` where documento=\'Factura de Venta\' and DATE(created_at) = CURDATE()\"}', '2017-03-17 23:57:39', NULL),
(4, 1, 'c5f1401d2c5f2528efacf5e2601481af', 'smallbox', 'area4', 0, NULL, '{\"name\":\"Total Factura de Venta Actuales\",\"icon\":\"ion-bag\",\"color\":\"bg-red\",\"link\":\"#\",\"sql\":\"select count(id) from `orders_fv` where documento=\'Factura de Venta\' and DATE(created_at) = CURDATE()\"}', '2017-03-17 23:59:07', NULL),
(5, 1, '6b8643480ea7e4763a8a9279496808a1', 'chartline', 'area5', 0, NULL, '{\"name\":\"Factura de Venta de este a\\u00f1o\",\"sql\":\"select date(created_at) as label, count(id) as value from `orders_fv` where documento=\'Factura de Venta\' and year(created_at) = YEAR(CURDATE()) group by label\",\"area_name\":\"Total Order\",\"goals\":\"\"}', '2017-03-18 00:00:18', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_users`
--

CREATE TABLE `cms_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `cms_users`
--

INSERT INTO `cms_users` (`id`, `name`, `photo`, `email`, `password`, `id_cms_privileges`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Super Admin', 'uploads/1/2017-10/images.jpg', 'admin@crudbooster.com', '$2y$10$BEJ4nVIhScimJ0Fybxl2ZucRSmAttP6iZ9/BytOg60kG3.chYzHPm', 1, '2017-10-10 08:33:30', '2017-10-11 02:04:41', 'Active'),
(2, 'Super Admin', 'uploads/1/2017-10/images.jpg', 'super@admin.com', '$2y$10$dwoYC63zPnq9flhzdHRIUue7LAsLOWF3T/2PHhjdXrwh5UF5Q2Zj2', 1, '2017-10-10 08:33:43', '2017-10-11 02:04:18', 'Active'),
(3, 'Demo', 'uploads/1/2017-10/images.jpg', 'demo@admin.com', '$2y$10$UUO7EQbWRiWlSwKSr.R2/OumNNxXzbkdPcfSIRR.tk3.uR5VgTzV.', 2, '2017-10-10 21:57:45', '2017-10-11 02:04:32', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contactos`
--

CREATE TABLE `contactos` (
  `id` int(10) UNSIGNED NOT NULL,
  `tipo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `documento` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion` text COLLATE utf8_unicode_ci,
  `contacto_nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contacto_telefono` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contacto_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contacto_detalles` text COLLATE utf8_unicode_ci,
  `opcional` text COLLATE utf8_unicode_ci,
  `opcional1` text COLLATE utf8_unicode_ci,
  `opcional2` text COLLATE utf8_unicode_ci,
  `opcional3` text COLLATE utf8_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `contactos`
--

INSERT INTO `contactos` (`id`, `tipo`, `documento`, `name`, `telefono`, `email`, `direccion`, `contacto_nombre`, `contacto_telefono`, `contacto_email`, `contacto_detalles`, `opcional`, `opcional1`, `opcional2`, `opcional3`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Cliente', 'J-40936247-7', 'Dolcegrotta888 C.A', '0212-6817608', 'dolcegrotta888@gmail.com', 'Av intercomunal del Valle, Calle Nº 10, Casa Nº 23, PB, Sector Los Jardines del Valle Caracas', 'Norelis', '0212-6817608', 'dolcegrotta888@gmail.com', 'bapowuv@gmail.com', 'In minim maxime labore beatae ullamco', NULL, NULL, NULL, NULL, '2017-10-30 18:11:13', '2017-11-01 19:44:17'),
(2, 'Proveedor', '23032986225', 'Montana Kirk', '+524-41-7360257', 'kijizyz@gmail.com', 'Facilis magni rem libero totam id similique', 'Ex minima blanditiis excepteur earum numquam minus pariatur', '+885-29-8354468', 'fylej@yahoo.com', 'byzoryke@gmail.com', '2222222', NULL, NULL, NULL, NULL, '2017-11-04 16:33:26', NULL),
(3, 'Cliente', '000234545', 'Robert Padovani', '58 212-361.26.68 ', 'padovani@tecvemar.net', 'Guatire', 'Robert Padovani', '58 212-361.26.68 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-06 19:42:24', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentas`
--

CREATE TABLE `cuentas` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `contactos_id` int(11) DEFAULT NULL,
  `cuentas_categorias_id` int(11) DEFAULT NULL,
  `tax` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `moneda` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` text COLLATE utf8_unicode_ci,
  `documento` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_numero` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sub_total` double NOT NULL DEFAULT '0',
  `tax_total` double NOT NULL DEFAULT '0',
  `grand_total` double NOT NULL DEFAULT '0',
  `enviado` double NOT NULL DEFAULT '0',
  `detalle` text COLLATE utf8_unicode_ci,
  `opcional` text COLLATE utf8_unicode_ci,
  `opcional1` text COLLATE utf8_unicode_ci,
  `opcional2` text COLLATE utf8_unicode_ci,
  `opcional3` text COLLATE utf8_unicode_ci,
  `es_activo` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentas_categorias`
--

CREATE TABLE `cuentas_categorias` (
  `id` int(10) UNSIGNED NOT NULL,
  `tipo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sku` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `saldo` double NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `cuentas_categorias`
--

INSERT INTO `cuentas_categorias` (`id`, `tipo`, `sku`, `name`, `description`, `saldo`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Caja', NULL, 'CajaDemo', 'prueba', 0, NULL, '2017-10-31 04:40:26', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentas_detail`
--

CREATE TABLE `cuentas_detail` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `items_orders_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `items_orders_doc` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `items_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `items_sku` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cuentas_categorias_id` int(11) DEFAULT NULL,
  `precio` double NOT NULL DEFAULT '0',
  `cantidad` double NOT NULL DEFAULT '0',
  `total` double NOT NULL DEFAULT '0',
  `enviado` double NOT NULL DEFAULT '0',
  `es_activo` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario`
--

CREATE TABLE `inventario` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `contactos_id` int(11) DEFAULT NULL,
  `tax` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `moneda` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `documento` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_numero` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sub_total` double NOT NULL DEFAULT '0',
  `tax_total` double NOT NULL DEFAULT '0',
  `detalle` text COLLATE utf8_unicode_ci,
  `grand_total` double NOT NULL DEFAULT '0',
  `descripcion` text COLLATE utf8_unicode_ci,
  `opcional` text COLLATE utf8_unicode_ci,
  `opcional1` text COLLATE utf8_unicode_ci,
  `opcional2` text COLLATE utf8_unicode_ci,
  `opcional3` text COLLATE utf8_unicode_ci,
  `enviado` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario_detail`
--

CREATE TABLE `inventario_detail` (
  `id` int(10) UNSIGNED NOT NULL,
  `items_orders_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `items_orders_doc` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `items_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `items_sku` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `precio` double NOT NULL DEFAULT '0',
  `cantidad` double NOT NULL DEFAULT '0',
  `total` double NOT NULL DEFAULT '0',
  `enviado` double NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2016_08_07_145904_add_table_cms_apicustom', 1),
(2, '2016_08_07_150834_add_table_cms_dashboard', 1),
(3, '2016_08_07_151210_add_table_cms_logs', 1),
(4, '2016_08_07_151211_add_details_cms_logs', 1),
(5, '2016_08_07_152014_add_table_cms_privileges', 1),
(6, '2016_08_07_152214_add_table_cms_privileges_roles', 1),
(7, '2016_08_07_152320_add_table_cms_settings', 1),
(8, '2016_08_07_152421_add_table_cms_users', 1),
(9, '2016_08_07_154624_add_table_cms_menus_privileges', 1),
(10, '2016_08_07_154624_add_table_cms_moduls', 1),
(11, '2016_08_17_225409_add_status_cms_users', 1),
(12, '2016_08_20_125418_add_table_cms_notifications', 1),
(13, '2016_09_04_033706_add_table_cms_email_queues', 1),
(14, '2016_09_16_035347_add_group_setting', 1),
(15, '2016_09_16_045425_add_label_setting', 1),
(16, '2016_09_17_104728_create_nullable_cms_apicustom', 1),
(17, '2016_10_01_141740_add_method_type_apicustom', 1),
(18, '2016_10_01_141846_add_parameters_apicustom', 1),
(19, '2016_10_01_141934_add_responses_apicustom', 1),
(20, '2016_10_01_144826_add_table_apikey', 1),
(21, '2016_11_14_141657_create_cms_menus', 1),
(22, '2016_11_15_132350_create_cms_email_templates', 1),
(23, '2016_11_15_190410_create_cms_statistics', 1),
(24, '2016_11_17_102740_create_cms_statistic_components', 1),
(25, '2017_03_17_085044_create_orders', 1),
(26, '2017_03_17_085104_create_orders_detail', 1),
(27, '2017_06_06_164501_add_deleted_at_cms_moduls', 1),
(28, '2017_10_07_183837_create_contactos', 1),
(29, '2017_10_09_183423_create_articulos', 1),
(30, '2017_10_09_183921_create_articulos_categorias', 1),
(31, '2017_10_14_085044_create_inventario', 1),
(32, '2017_10_14_085104_create_inventario_detail', 1),
(33, '2017_10_14_085105_create_movimientos_detail', 1),
(34, '2017_10_16_085044_create_cuentas', 1),
(35, '2017_10_16_085104_create_cuentas_detail', 1),
(36, '2017_10_16_085105_create_movimientos_cuentas_detail', 1),
(37, '2017_10_16_183921_create_cuentas_categorias', 1),
(38, '2017_10_30_085044_create_orders_fv', 1),
(39, '2017_10_30_085104_create_orders_detail_fv', 1),
(40, '2017_10_30_183921_create_vendedor', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movimientos_cuentas_detail`
--

CREATE TABLE `movimientos_cuentas_detail` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `items_orders_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `items_orders_doc` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `items_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `items_sku` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cuentas_categorias_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `precio` double NOT NULL DEFAULT '0',
  `cantidad` double NOT NULL DEFAULT '0',
  `total` double NOT NULL DEFAULT '0',
  `enviado` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movimientos_detail`
--

CREATE TABLE `movimientos_detail` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `items_orders_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `items_orders_doc` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `items_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `items_sku` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `items_tax` double NOT NULL DEFAULT '0',
  `precio` double NOT NULL DEFAULT '0',
  `cantidad` double NOT NULL DEFAULT '0',
  `total_mas_tax` double NOT NULL DEFAULT '0',
  `total` double NOT NULL DEFAULT '0',
  `enviado` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `movimientos_detail`
--

INSERT INTO `movimientos_detail` (`id`, `deleted_at`, `items_orders_id`, `items_orders_doc`, `items_id`, `items_sku`, `name`, `items_tax`, `precio`, `cantidad`, `total_mas_tax`, `total`, `enviado`) VALUES
(1, NULL, '00001', 'Factura de Venta', '1', '171354949542', 'Computadora', 0, 45, -1, 0, 45, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `contactos_id` int(11) DEFAULT NULL,
  `vendedor_id` int(11) DEFAULT NULL,
  `tax` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `moneda` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `documento` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_numero` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `numero_de_control` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sub_total` double NOT NULL DEFAULT '0',
  `tax_total` double NOT NULL DEFAULT '0',
  `detalle` text COLLATE utf8_unicode_ci,
  `grand_total` double NOT NULL DEFAULT '0',
  `saldo` double NOT NULL DEFAULT '0',
  `enviado` double NOT NULL DEFAULT '0',
  `vence` date DEFAULT NULL,
  `opcional` text COLLATE utf8_unicode_ci,
  `opcional1` text COLLATE utf8_unicode_ci,
  `opcional2` text COLLATE utf8_unicode_ci,
  `opcional3` text COLLATE utf8_unicode_ci,
  `es_activo` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders_detail`
--

CREATE TABLE `orders_detail` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `items_orders_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `items_orders_doc` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `items_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `items_sku` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `items_tax` double NOT NULL DEFAULT '0',
  `precio` double NOT NULL DEFAULT '0',
  `cantidad` double NOT NULL DEFAULT '0',
  `total` double NOT NULL DEFAULT '0',
  `total_mas_tax` double NOT NULL DEFAULT '0',
  `enviado` double NOT NULL DEFAULT '0',
  `es_activo` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders_detail_fv`
--

CREATE TABLE `orders_detail_fv` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `items_orders_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `items_orders_doc` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `items_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `items_sku` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `items_tax` double NOT NULL DEFAULT '0',
  `precio` double NOT NULL DEFAULT '0',
  `cantidad` double NOT NULL DEFAULT '0',
  `total` double NOT NULL DEFAULT '0',
  `total_mas_tax` double NOT NULL DEFAULT '0',
  `enviado` double NOT NULL DEFAULT '0',
  `es_activo` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `orders_detail_fv`
--

INSERT INTO `orders_detail_fv` (`id`, `deleted_at`, `items_orders_id`, `items_orders_doc`, `items_id`, `items_sku`, `name`, `items_tax`, `precio`, `cantidad`, `total`, `total_mas_tax`, `enviado`, `es_activo`) VALUES
(1, NULL, '', 'Factura de Venta', '1', '171354949542', 'Computadora', 12, 45, 1, 45, 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders_fv`
--

CREATE TABLE `orders_fv` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `contactos_id` int(11) DEFAULT NULL,
  `vendedor_id` int(11) DEFAULT NULL,
  `tax` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `moneda` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `documento` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_numero` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `numero_de_control` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sub_total` double NOT NULL DEFAULT '0',
  `tax_total` double NOT NULL DEFAULT '0',
  `detalle` text COLLATE utf8_unicode_ci,
  `grand_total` double NOT NULL DEFAULT '0',
  `saldo` double NOT NULL DEFAULT '0',
  `enviado` double NOT NULL DEFAULT '0',
  `vence` date DEFAULT NULL,
  `opcional` text COLLATE utf8_unicode_ci,
  `opcional1` text COLLATE utf8_unicode_ci,
  `opcional2` text COLLATE utf8_unicode_ci,
  `opcional3` text COLLATE utf8_unicode_ci,
  `es_activo` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `orders_fv`
--

INSERT INTO `orders_fv` (`id`, `created_at`, `updated_at`, `deleted_at`, `contactos_id`, `vendedor_id`, `tax`, `moneda`, `documento`, `order_numero`, `numero_de_control`, `sub_total`, `tax_total`, `detalle`, `grand_total`, `saldo`, `enviado`, `vence`, `opcional`, `opcional1`, `opcional2`, `opcional3`, `es_activo`) VALUES
(1, '2018-01-23 00:27:24', NULL, NULL, 3, 1, '9', 'Bs', 'Factura de Venta', '00001', NULL, 45, 4.05, '[{\"items_orders_id\":\"\",\"items_orders_doc\":\"Factura de Venta\",\"items_id\":1,\"items_sku\":\"171354949542\",\"items_tax\":12,\"name\":\"Computadora\",\"precio\":45,\"cantidad\":\"1\",\"total\":45}]', 49.05, 49.05, 0, '2018-01-22', '12345646648', '234234', NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vendedor`
--

CREATE TABLE `vendedor` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comision` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `opcional` text COLLATE utf8_unicode_ci,
  `opcional1` text COLLATE utf8_unicode_ci,
  `opcional2` text COLLATE utf8_unicode_ci,
  `opcional3` text COLLATE utf8_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `vendedor`
--

INSERT INTO `vendedor` (`id`, `name`, `comision`, `opcional`, `opcional1`, `opcional2`, `opcional3`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'VenDemo', '0', NULL, NULL, NULL, NULL, NULL, '2017-10-30 18:09:33', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `articulos`
--
ALTER TABLE `articulos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `articulos_categorias`
--
ALTER TABLE `articulos_categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_apicustom`
--
ALTER TABLE `cms_apicustom`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_apikey`
--
ALTER TABLE `cms_apikey`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_dashboard`
--
ALTER TABLE `cms_dashboard`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_email_queues`
--
ALTER TABLE `cms_email_queues`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_email_templates`
--
ALTER TABLE `cms_email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_logs`
--
ALTER TABLE `cms_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_menus`
--
ALTER TABLE `cms_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_menus_privileges`
--
ALTER TABLE `cms_menus_privileges`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_moduls`
--
ALTER TABLE `cms_moduls`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_notifications`
--
ALTER TABLE `cms_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_privileges`
--
ALTER TABLE `cms_privileges`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_privileges_roles`
--
ALTER TABLE `cms_privileges_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_settings`
--
ALTER TABLE `cms_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_statistics`
--
ALTER TABLE `cms_statistics`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_statistic_components`
--
ALTER TABLE `cms_statistic_components`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_users`
--
ALTER TABLE `cms_users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contactos`
--
ALTER TABLE `contactos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cuentas`
--
ALTER TABLE `cuentas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cuentas_categorias`
--
ALTER TABLE `cuentas_categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cuentas_detail`
--
ALTER TABLE `cuentas_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `inventario`
--
ALTER TABLE `inventario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `inventario_detail`
--
ALTER TABLE `inventario_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `movimientos_cuentas_detail`
--
ALTER TABLE `movimientos_cuentas_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `movimientos_detail`
--
ALTER TABLE `movimientos_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `orders_detail`
--
ALTER TABLE `orders_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `orders_detail_fv`
--
ALTER TABLE `orders_detail_fv`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `orders_fv`
--
ALTER TABLE `orders_fv`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `vendedor`
--
ALTER TABLE `vendedor`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `articulos`
--
ALTER TABLE `articulos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `articulos_categorias`
--
ALTER TABLE `articulos_categorias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `cms_apicustom`
--
ALTER TABLE `cms_apicustom`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cms_apikey`
--
ALTER TABLE `cms_apikey`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cms_dashboard`
--
ALTER TABLE `cms_dashboard`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cms_email_queues`
--
ALTER TABLE `cms_email_queues`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cms_email_templates`
--
ALTER TABLE `cms_email_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cms_logs`
--
ALTER TABLE `cms_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cms_menus`
--
ALTER TABLE `cms_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT de la tabla `cms_menus_privileges`
--
ALTER TABLE `cms_menus_privileges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT de la tabla `cms_moduls`
--
ALTER TABLE `cms_moduls`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `cms_notifications`
--
ALTER TABLE `cms_notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cms_privileges`
--
ALTER TABLE `cms_privileges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `cms_privileges_roles`
--
ALTER TABLE `cms_privileges_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT de la tabla `cms_settings`
--
ALTER TABLE `cms_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `cms_statistics`
--
ALTER TABLE `cms_statistics`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `cms_statistic_components`
--
ALTER TABLE `cms_statistic_components`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `cms_users`
--
ALTER TABLE `cms_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `contactos`
--
ALTER TABLE `contactos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `cuentas`
--
ALTER TABLE `cuentas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cuentas_categorias`
--
ALTER TABLE `cuentas_categorias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `cuentas_detail`
--
ALTER TABLE `cuentas_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `inventario`
--
ALTER TABLE `inventario`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `inventario_detail`
--
ALTER TABLE `inventario_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT de la tabla `movimientos_cuentas_detail`
--
ALTER TABLE `movimientos_cuentas_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `movimientos_detail`
--
ALTER TABLE `movimientos_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `orders_detail`
--
ALTER TABLE `orders_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `orders_detail_fv`
--
ALTER TABLE `orders_detail_fv`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `orders_fv`
--
ALTER TABLE `orders_fv`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `vendedor`
--
ALTER TABLE `vendedor`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
