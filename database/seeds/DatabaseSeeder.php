<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $this->call('Cms_usersSeeder');  
     $this->call('cms_settings');    
    }


}



class Cms_usersSeeder extends Seeder {

    public function run()
    {        
        
        if(DB::table('cms_users')->count() == 1) {
            $password = \Hash::make('123456');
            $cms_users = DB::table('cms_users')->insert(array(
                'id'                =>DB::table('cms_users')->max('id')+1,
                'created_at'        =>date('Y-m-d H:i:s'),
                'name'              => 'Super Admin',                
                'email'             => 'super@admin.com',
                'password'          => $password,
                'id_cms_privileges' => 1,                
                'status'            =>'Active'
            ));
        }            

    }

}

class cms_settings extends Seeder {

    public function run()
    {        

/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 4.7.4
 */

/**
 * Database `gealight`
 */

/* `gealight`.`articulos` */
$articulos = array(
  array('id' => '1','sku' => '171354949542','name' => 'Computadora','description' => 'Et molestiae debitis incididunt fugiat ea quia architecto debitis similique dolor doloremque sequi magnam nemo','precio' => '45','precio_compra' => '5','stock' => '6','tax' => '12','unidad_de_medida' => NULL,'articulos_categorias_id' => '1','photo' => NULL,'photo1' => NULL,'opcional' => 'Non saepe obcaecati autem officiis cum mollitia reprehenderit irure nulla tempora magnam','opcional1' => 'Sed ut quis sunt necessitatibus temporibus itaque dicta minima nihil iure quas','opcional2' => NULL,'opcional3' => NULL,'deleted_at' => NULL,'created_at' => '2017-10-30 14:11:49','updated_at' => NULL),
  array('id' => '2','sku' => '315123959682','name' => 'Libby Riggs','description' => 'Ipsam perferendis molestiae amet esse et id elit voluptatem Esse sint fuga Sit eius pariatur Aut omnis ea','precio' => '53','precio_compra' => '42','stock' => '0','tax' => '45','unidad_de_medida' => NULL,'articulos_categorias_id' => '1','photo' => NULL,'photo1' => NULL,'opcional' => 'Nulla architecto lorem dolore maiores similique nulla provident molestiae minim nihil','opcional1' => 'Vero quo ad saepe dolorem mollitia et iste nesciunt','opcional2' => NULL,'opcional3' => NULL,'deleted_at' => NULL,'created_at' => '2017-11-04 12:40:16','updated_at' => NULL),
  array('id' => '3','sku' => '98874654','name' => 'Dominio con extensión .com','description' => 'Dominio con extensión .com','precio' => '850000','precio_compra' => '572500','stock' => '0','tax' => '12','unidad_de_medida' => NULL,'articulos_categorias_id' => '1','photo' => NULL,'photo1' => NULL,'opcional' => NULL,'opcional1' => NULL,'opcional2' => NULL,'opcional3' => NULL,'deleted_at' => NULL,'created_at' => '2017-11-06 15:43:21','updated_at' => NULL),
  array('id' => '4','sku' => '423423','name' => 'Hosting Ilimitado Periodicidad trimestral','description' => 'Hosting Ilimitado Periodicidad trimestral','precio' => '450000','precio_compra' => '449999','stock' => '0','tax' => '12','unidad_de_medida' => NULL,'articulos_categorias_id' => '1','photo' => NULL,'photo1' => NULL,'opcional' => NULL,'opcional1' => NULL,'opcional2' => NULL,'opcional3' => NULL,'deleted_at' => NULL,'created_at' => '2017-11-06 15:45:34','updated_at' => NULL)
);

/* `gealight`.`articulos_categorias` */
$articulos_categorias = array(
  array('id' => '1','name' => 'CatDemo','deleted_at' => NULL,'created_at' => '2017-10-30 14:09:51','updated_at' => NULL)
);

/* `gealight`.`cms_apicustom` */
$cms_apicustom = array(
);

/* `gealight`.`cms_apikey` */
$cms_apikey = array(
);

/* `gealight`.`cms_dashboard` */
$cms_dashboard = array(
);

/* `gealight`.`cms_email_queues` */
$cms_email_queues = array(
);

/* `gealight`.`cms_email_templates` */
$cms_email_templates = array(
);

/* `gealight`.`cms_logs` */
$cms_logs = array(
  array('id' => '1','ipaddress' => '::1','useragent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36','url' => 'http://localhost/gea-light/public/admin/menu_management/edit-save/31','description' => 'Actualizar información Cotizaciones en Gestión de Menús','details' => '<table class="table table-striped"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>sorting</td><td>8</td><td></td></tr></tbody></table>','id_cms_users' => '1','created_at' => '2017-12-10 15:46:31','updated_at' => NULL),
  array('id' => '2','ipaddress' => '::1','useragent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36','url' => 'http://localhost/gea-light/public/admin/menu_management/edit-save/31','description' => 'Actualizar información Cotizaciones en Gestión de Menús','details' => '<table class="table table-striped"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>icon</td><td>fa fa-plus</td><td>fa fa-shopping-cart</td></tr><tr><td>sorting</td><td>8</td><td></td></tr></tbody></table>','id_cms_users' => '1','created_at' => '2017-12-10 15:49:16','updated_at' => NULL),
  array('id' => '3','ipaddress' => '::1','useragent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36','url' => 'http://localhost/gea-light/public/admin/orders_cotizacion/delete/1','description' => 'Eliminar información 1 en Cotizaciones','details' => '','id_cms_users' => '1','created_at' => '2017-12-10 16:26:11','updated_at' => NULL),
  array('id' => '4','ipaddress' => '::1','useragent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36','url' => 'http://localhost/gea-light/public/admin/menu_management/add-save','description' => 'Añadir nueva información Nueva Cotización en Gestión de Menús','details' => '','id_cms_users' => '1','created_at' => '2017-12-10 16:30:37','updated_at' => NULL),
  array('id' => '5','ipaddress' => '::1','useragent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36','url' => 'http://localhost/gea-light/public/admin/menu_management/edit-save/33','description' => 'Actualizar información Compras en Gestión de Menús','details' => '<table class="table table-striped"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>name</td><td>Compra</td><td>Compras</td></tr><tr><td>color</td><td></td><td>normal</td></tr><tr><td>parent_id</td><td>28</td><td></td></tr><tr><td>sorting</td><td>4</td><td></td></tr></tbody></table>','id_cms_users' => '1','created_at' => '2017-12-10 17:05:48','updated_at' => NULL),
  array('id' => '6','ipaddress' => '::1','useragent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36','url' => 'http://localhost/gea-light/public/admin/menu_management/add-save','description' => 'Añadir nueva información Nueva Compra en Gestión de Menús','details' => '','id_cms_users' => '1','created_at' => '2017-12-10 17:08:56','updated_at' => NULL),
  array('id' => '7','ipaddress' => '::1','useragent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36','url' => 'http://localhost/gea-light/public/admin/menu_management/delete/4','description' => 'Eliminar información Ordenes en Gestión de Menús','details' => '','id_cms_users' => '1','created_at' => '2017-12-10 17:10:04','updated_at' => NULL),
  array('id' => '8','ipaddress' => '::1','useragent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36','url' => 'http://localhost/gea-light/public/admin/movimientos_detail18/delete/1','description' => 'Eliminar información 1 en Movimientos','details' => '','id_cms_users' => '1','created_at' => '2017-12-10 17:28:02','updated_at' => NULL),
  array('id' => '9','ipaddress' => '::1','useragent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36','url' => 'http://localhost/gea-light/public/admin/logout','description' => 'admin@crudbooster.com se desconectó','details' => '','id_cms_users' => '1','created_at' => '2017-12-10 17:29:27','updated_at' => NULL),
  array('id' => '10','ipaddress' => '::1','useragent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36','url' => 'http://localhost/gea-light/public/admin/login','description' => 'Ingreso de demo@admin.com desde la Dirección IP ::1','details' => '','id_cms_users' => '3','created_at' => '2017-12-10 17:29:33','updated_at' => NULL),
  array('id' => '11','ipaddress' => '::1','useragent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36','url' => 'http://localhost/gea-light/public/admin/orders_cotizacion','description' => 'Intentar ver :name en Cotizaciones','details' => '','id_cms_users' => '3','created_at' => '2017-12-10 17:29:52','updated_at' => NULL),
  array('id' => '12','ipaddress' => '::1','useragent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36','url' => 'http://localhost/gea-light/public/admin/orders_compra','description' => 'Intentar ver :name en Compra','details' => '','id_cms_users' => '3','created_at' => '2017-12-10 17:29:57','updated_at' => NULL),
  array('id' => '13','ipaddress' => '::1','useragent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36','url' => 'http://localhost/gea-light/public/admin/logout','description' => 'demo@admin.com se desconectó','details' => '','id_cms_users' => '3','created_at' => '2017-12-10 17:30:03','updated_at' => NULL),
  array('id' => '14','ipaddress' => '::1','useragent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36','url' => 'http://localhost/gea-light/public/admin/login','description' => 'Ingreso de admin@crudbooster.com desde la Dirección IP ::1','details' => '','id_cms_users' => '1','created_at' => '2017-12-10 17:30:06','updated_at' => NULL),
  array('id' => '15','ipaddress' => '::1','useragent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36','url' => 'http://localhost/gea-light/public/admin/logout','description' => 'admin@crudbooster.com se desconectó','details' => '','id_cms_users' => '1','created_at' => '2017-12-10 17:30:38','updated_at' => NULL),
  array('id' => '16','ipaddress' => '::1','useragent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36','url' => 'http://localhost/gea-light/public/admin/login','description' => 'Ingreso de admin@crudbooster.com desde la Dirección IP ::1','details' => '','id_cms_users' => '1','created_at' => '2017-12-10 17:30:57','updated_at' => NULL),
  array('id' => '17','ipaddress' => '::1','useragent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36','url' => 'http://localhost/gea-light/public/admin/logout','description' => 'admin@crudbooster.com se desconectó','details' => '','id_cms_users' => '1','created_at' => '2017-12-10 17:31:10','updated_at' => NULL),
  array('id' => '18','ipaddress' => '::1','useragent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36','url' => 'http://localhost/gea-light/public/admin/login','description' => 'Ingreso de demo@admin.com desde la Dirección IP ::1','details' => '','id_cms_users' => '3','created_at' => '2017-12-10 17:31:15','updated_at' => NULL),
  array('id' => '19','ipaddress' => '::1','useragent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36','url' => 'http://localhost/gea-light/public/admin/orders_cotizacion','description' => 'Intentar ver :name en Cotizaciones','details' => '','id_cms_users' => '3','created_at' => '2017-12-10 17:31:35','updated_at' => NULL),
  array('id' => '20','ipaddress' => '::1','useragent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36','url' => 'http://localhost/gea-light/public/admin/logout','description' => 'demo@admin.com se desconectó','details' => '','id_cms_users' => '3','created_at' => '2017-12-10 17:31:42','updated_at' => NULL),
  array('id' => '21','ipaddress' => '::1','useragent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36','url' => 'http://localhost/gea-light/public/admin/logout','description' => 'demo@admin.com se desconectó','details' => '','id_cms_users' => '3','created_at' => '2017-12-10 17:31:53','updated_at' => NULL),
  array('id' => '22','ipaddress' => '::1','useragent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36','url' => 'http://localhost/gea-light/public/admin/login','description' => 'Ingreso de admin@crudbooster.com desde la Dirección IP ::1','details' => '','id_cms_users' => '1','created_at' => '2017-12-10 17:31:56','updated_at' => NULL),
  array('id' => '23','ipaddress' => '::1','useragent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36','url' => 'http://localhost/gea-light/public/admin/logout','description' => 'admin@crudbooster.com se desconectó','details' => '','id_cms_users' => '1','created_at' => '2017-12-10 17:33:22','updated_at' => NULL),
  array('id' => '24','ipaddress' => '::1','useragent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36','url' => 'http://localhost/gea-light/public/admin/login','description' => 'Ingreso de demo@admin.com desde la Dirección IP ::1','details' => '','id_cms_users' => '3','created_at' => '2017-12-10 17:33:28','updated_at' => NULL)
);

/* `gealight`.`cms_menus` */
$cms_menus = array(
  array('id' => '2','name' => 'Nueva Categoría Art','type' => 'Route','path' => 'AdminArticulosCategoriasControllerGetIndex','color' => 'normal','icon' => 'fa fa-plus','parent_id' => '15','is_active' => '1','is_dashboard' => '0','id_cms_privileges' => '1','sorting' => '4','created_at' => '2017-10-09 23:24:21','updated_at' => '2017-12-10 00:12:33'),
  array('id' => '3','name' => 'Nuevo Artículos','type' => 'Route','path' => 'AdminArticulosControllerGetIndex','color' => 'normal','icon' => 'fa fa-plus','parent_id' => '15','is_active' => '1','is_dashboard' => '0','id_cms_privileges' => '1','sorting' => '5','created_at' => '2017-10-09 23:25:27','updated_at' => '2017-12-10 00:06:17'),
  array('id' => '5','name' => 'GeaLigth','type' => 'Route','path' => 'index','color' => 'light-blue','icon' => 'fa fa-home','parent_id' => '0','is_active' => '1','is_dashboard' => '0','id_cms_privileges' => '1','sorting' => '2','created_at' => '2017-10-10 06:23:10','updated_at' => '2017-10-27 06:12:33'),
  array('id' => '6','name' => 'Inventario','type' => 'Route','path' => 'AdminInventarioControllerGetIndex','color' => NULL,'icon' => 'fa fa-cube','parent_id' => '29','is_active' => '1','is_dashboard' => '0','id_cms_privileges' => '1','sorting' => '1','created_at' => '2017-10-14 14:50:18','updated_at' => NULL),
  array('id' => '8','name' => 'Inventarios mov','type' => 'Route','path' => 'AdminMovimientosDetail18ControllerGetIndex','color' => 'normal','icon' => 'fa fa-info','parent_id' => '29','is_active' => '1','is_dashboard' => '0','id_cms_privileges' => '1','sorting' => '2','created_at' => '2017-10-14 15:30:37','updated_at' => '2017-12-09 23:54:32'),
  array('id' => '9','name' => 'Nueva Caja y Bancos','type' => 'Route','path' => 'AdminCuentasCategoriasControllerGetIndex','color' => 'normal','icon' => 'fa fa-plus','parent_id' => '15','is_active' => '1','is_dashboard' => '0','id_cms_privileges' => '1','sorting' => '6','created_at' => '2017-10-16 14:00:18','updated_at' => '2017-12-10 00:03:21'),
  array('id' => '10','name' => 'Caja y Banco Mov','type' => 'Route','path' => 'AdminMovimientosCuentasDetailControllerGetIndex','color' => 'normal','icon' => 'fa fa-info','parent_id' => '29','is_active' => '1','is_dashboard' => '0','id_cms_privileges' => '1','sorting' => '4','created_at' => '2017-10-16 14:04:01','updated_at' => '2017-12-09 23:55:17'),
  array('id' => '11','name' => 'Caja y Bancos','type' => 'Route','path' => 'AdminCuentasControllerGetIndex','color' => 'normal','icon' => 'fa fa-dollar','parent_id' => '29','is_active' => '1','is_dashboard' => '0','id_cms_privileges' => '1','sorting' => '3','created_at' => '2017-10-16 15:26:47','updated_at' => '2017-12-09 23:56:20'),
  array('id' => '15','name' => 'Procesos','type' => 'URL','path' => '#','color' => 'normal','icon' => 'fa fa-th-list','parent_id' => '0','is_active' => '1','is_dashboard' => '0','id_cms_privileges' => '1','sorting' => '4','created_at' => '2017-10-27 11:55:24','updated_at' => NULL),
  array('id' => '16','name' => 'Nueva Factura','type' => 'Route','path' => 'index','color' => 'normal','icon' => 'fa fa-plus','parent_id' => '15','is_active' => '1','is_dashboard' => '0','id_cms_privileges' => '1','sorting' => '1','created_at' => '2017-10-27 11:57:19','updated_at' => '2017-12-09 23:29:29'),
  array('id' => '17','name' => 'Cargar o  sacar de Inventario','type' => 'Route','path' => 'inventarios','color' => 'normal','icon' => 'fa fa-cubes','parent_id' => '15','is_active' => '1','is_dashboard' => '0','id_cms_privileges' => '1','sorting' => '9','created_at' => '2017-10-27 11:58:28','updated_at' => '2017-12-10 15:22:34'),
  array('id' => '18','name' => 'Gestionar Cajas y Bancos','type' => 'Route','path' => 'cuentas','color' => 'normal','icon' => 'fa fa-bank','parent_id' => '15','is_active' => '1','is_dashboard' => '0','id_cms_privileges' => '1','sorting' => '8','created_at' => '2017-10-27 11:59:13','updated_at' => '2017-12-10 00:01:53'),
  array('id' => '19','name' => 'Dashboard','type' => 'Statistic','path' => 'statistic_builder/show/dashboard','color' => 'normal','icon' => 'fa fa-dashboard','parent_id' => '0','is_active' => '1','is_dashboard' => '1','id_cms_privileges' => '1','sorting' => '1','created_at' => '2017-10-27 12:10:25','updated_at' => NULL),
  array('id' => '23','name' => 'Clientes','type' => 'Route','path' => 'AdminClientesControllerGetIndex','color' => 'normal','icon' => 'fa fa-plus','parent_id' => '25','is_active' => '1','is_dashboard' => '0','id_cms_privileges' => '1','sorting' => '1','created_at' => '2017-10-29 23:08:25','updated_at' => '2017-12-08 18:39:47'),
  array('id' => '24','name' => 'Proveedores','type' => 'Route','path' => 'AdminProveedoresControllerGetIndex','color' => 'normal','icon' => 'fa fa-plus','parent_id' => '25','is_active' => '1','is_dashboard' => '0','id_cms_privileges' => '1','sorting' => '2','created_at' => '2017-10-29 23:11:08','updated_at' => '2017-12-08 18:40:15'),
  array('id' => '25','name' => 'Clientes Y Proveedores','type' => 'URL','path' => '#','color' => 'normal','icon' => 'fa fa-users','parent_id' => '0','is_active' => '1','is_dashboard' => '0','id_cms_privileges' => '1','sorting' => '3','created_at' => '2017-10-29 23:12:43','updated_at' => '2017-12-08 18:39:26'),
  array('id' => '26','name' => 'Nuevo Vendedor','type' => 'Route','path' => 'AdminVendedorControllerGetIndex','color' => 'normal','icon' => 'fa fa-plus','parent_id' => '15','is_active' => '1','is_dashboard' => '0','id_cms_privileges' => '1','sorting' => '7','created_at' => '2017-10-30 11:09:48','updated_at' => '2017-12-10 00:03:38'),
  array('id' => '27','name' => 'Facturas','type' => 'Route','path' => 'AdminFacturasControllerGetIndex','color' => 'normal','icon' => 'fa fa-shopping-cart','parent_id' => '28','is_active' => '1','is_dashboard' => '0','id_cms_privileges' => '1','sorting' => '2','created_at' => '2017-10-30 13:54:24','updated_at' => '2017-10-30 14:02:49'),
  array('id' => '28','name' => 'Reportes','type' => 'URL','path' => '#','color' => 'normal','icon' => 'fa fa-stack-overflow','parent_id' => '0','is_active' => '1','is_dashboard' => '0','id_cms_privileges' => '1','sorting' => '5','created_at' => '2017-12-09 23:43:53','updated_at' => NULL),
  array('id' => '29','name' => 'Movimientos','type' => 'URL','path' => '#','color' => 'normal','icon' => 'fa fa-sort','parent_id' => '0','is_active' => '1','is_dashboard' => '0','id_cms_privileges' => '1','sorting' => '6','created_at' => '2017-12-09 23:53:17','updated_at' => NULL),
  array('id' => '30','name' => 'Inventario actual','type' => 'Module','path' => 'articulos','color' => 'normal','icon' => 'fa fa-cubes','parent_id' => '28','is_active' => '1','is_dashboard' => '0','id_cms_privileges' => '1','sorting' => '1','created_at' => '2017-12-10 15:26:12','updated_at' => NULL),
  array('id' => '31','name' => 'Cotizaciones','type' => 'Route','path' => 'AdminOrdersCotizacionControllerGetIndex','color' => 'normal','icon' => 'fa fa-shopping-cart','parent_id' => '28','is_active' => '1','is_dashboard' => '0','id_cms_privileges' => '1','sorting' => '3','created_at' => '2017-12-10 15:44:24','updated_at' => '2017-12-10 15:49:15'),
  array('id' => '32','name' => 'Nueva Cotización','type' => 'Route','path' => 'cotizacion','color' => 'normal','icon' => 'fa fa-plus','parent_id' => '15','is_active' => '1','is_dashboard' => '0','id_cms_privileges' => '1','sorting' => '2','created_at' => '2017-12-10 16:30:37','updated_at' => NULL),
  array('id' => '33','name' => 'Compras','type' => 'Route','path' => 'AdminOrdersCompraControllerGetIndex','color' => 'normal','icon' => 'fa fa-shopping-cart','parent_id' => '28','is_active' => '1','is_dashboard' => '0','id_cms_privileges' => '1','sorting' => '4','created_at' => '2017-12-10 16:51:10','updated_at' => '2017-12-10 17:05:48'),
  array('id' => '34','name' => 'Nueva Compra','type' => 'Route','path' => 'compra','color' => 'normal','icon' => 'fa fa-plus','parent_id' => '15','is_active' => '1','is_dashboard' => '0','id_cms_privileges' => '1','sorting' => '3','created_at' => '2017-12-10 17:08:55','updated_at' => NULL)
);

/* `gealight`.`cms_menus_privileges` */
$cms_menus_privileges = array(
  array('id' => '15','id_cms_menus' => '1','id_cms_privileges' => '2'),
  array('id' => '16','id_cms_menus' => '1','id_cms_privileges' => '1'),
  array('id' => '21','id_cms_menus' => '4','id_cms_privileges' => '2'),
  array('id' => '22','id_cms_menus' => '4','id_cms_privileges' => '1'),
  array('id' => '25','id_cms_menus' => '6','id_cms_privileges' => '2'),
  array('id' => '26','id_cms_menus' => '6','id_cms_privileges' => '1'),
  array('id' => '27','id_cms_menus' => '6','id_cms_privileges' => '1'),
  array('id' => '28','id_cms_menus' => '7','id_cms_privileges' => '1'),
  array('id' => '40','id_cms_menus' => '13','id_cms_privileges' => '2'),
  array('id' => '41','id_cms_menus' => '13','id_cms_privileges' => '1'),
  array('id' => '44','id_cms_menus' => '12','id_cms_privileges' => '2'),
  array('id' => '45','id_cms_menus' => '12','id_cms_privileges' => '1'),
  array('id' => '48','id_cms_menus' => '5','id_cms_privileges' => '2'),
  array('id' => '49','id_cms_menus' => '5','id_cms_privileges' => '1'),
  array('id' => '50','id_cms_menus' => '15','id_cms_privileges' => '2'),
  array('id' => '51','id_cms_menus' => '15','id_cms_privileges' => '1'),
  array('id' => '58','id_cms_menus' => '19','id_cms_privileges' => '2'),
  array('id' => '59','id_cms_menus' => '19','id_cms_privileges' => '1'),
  array('id' => '64','id_cms_menus' => '22','id_cms_privileges' => '2'),
  array('id' => '65','id_cms_menus' => '22','id_cms_privileges' => '1'),
  array('id' => '72','id_cms_menus' => '21','id_cms_privileges' => '2'),
  array('id' => '73','id_cms_menus' => '21','id_cms_privileges' => '1'),
  array('id' => '74','id_cms_menus' => '20','id_cms_privileges' => '2'),
  array('id' => '75','id_cms_menus' => '20','id_cms_privileges' => '1'),
  array('id' => '81','id_cms_menus' => '27','id_cms_privileges' => '2'),
  array('id' => '82','id_cms_menus' => '27','id_cms_privileges' => '1'),
  array('id' => '85','id_cms_menus' => '25','id_cms_privileges' => '2'),
  array('id' => '86','id_cms_menus' => '25','id_cms_privileges' => '1'),
  array('id' => '87','id_cms_menus' => '23','id_cms_privileges' => '2'),
  array('id' => '88','id_cms_menus' => '23','id_cms_privileges' => '1'),
  array('id' => '89','id_cms_menus' => '24','id_cms_privileges' => '2'),
  array('id' => '90','id_cms_menus' => '24','id_cms_privileges' => '1'),
  array('id' => '91','id_cms_menus' => '16','id_cms_privileges' => '2'),
  array('id' => '92','id_cms_menus' => '16','id_cms_privileges' => '1'),
  array('id' => '107','id_cms_menus' => '28','id_cms_privileges' => '2'),
  array('id' => '108','id_cms_menus' => '28','id_cms_privileges' => '1'),
  array('id' => '109','id_cms_menus' => '29','id_cms_privileges' => '1'),
  array('id' => '110','id_cms_menus' => '8','id_cms_privileges' => '1'),
  array('id' => '111','id_cms_menus' => '10','id_cms_privileges' => '1'),
  array('id' => '112','id_cms_menus' => '11','id_cms_privileges' => '2'),
  array('id' => '113','id_cms_menus' => '11','id_cms_privileges' => '1'),
  array('id' => '116','id_cms_menus' => '14','id_cms_privileges' => '2'),
  array('id' => '117','id_cms_menus' => '14','id_cms_privileges' => '1'),
  array('id' => '118','id_cms_menus' => '18','id_cms_privileges' => '2'),
  array('id' => '119','id_cms_menus' => '18','id_cms_privileges' => '1'),
  array('id' => '120','id_cms_menus' => '9','id_cms_privileges' => '1'),
  array('id' => '121','id_cms_menus' => '26','id_cms_privileges' => '2'),
  array('id' => '122','id_cms_menus' => '26','id_cms_privileges' => '1'),
  array('id' => '123','id_cms_menus' => '3','id_cms_privileges' => '2'),
  array('id' => '124','id_cms_menus' => '3','id_cms_privileges' => '1'),
  array('id' => '127','id_cms_menus' => '2','id_cms_privileges' => '2'),
  array('id' => '128','id_cms_menus' => '2','id_cms_privileges' => '1'),
  array('id' => '129','id_cms_menus' => '17','id_cms_privileges' => '1'),
  array('id' => '130','id_cms_menus' => '30','id_cms_privileges' => '2'),
  array('id' => '131','id_cms_menus' => '30','id_cms_privileges' => '1'),
  array('id' => '132','id_cms_menus' => '31','id_cms_privileges' => '2'),
  array('id' => '133','id_cms_menus' => '31','id_cms_privileges' => '1'),
  array('id' => '134','id_cms_menus' => '32','id_cms_privileges' => '2'),
  array('id' => '135','id_cms_menus' => '32','id_cms_privileges' => '1'),
  array('id' => '136','id_cms_menus' => '33','id_cms_privileges' => '2'),
  array('id' => '137','id_cms_menus' => '33','id_cms_privileges' => '1'),
  array('id' => '138','id_cms_menus' => '34','id_cms_privileges' => '2'),
  array('id' => '139','id_cms_menus' => '34','id_cms_privileges' => '1')
);

/* `gealight`.`cms_moduls` */
$cms_moduls = array(
  array('id' => '1','name' => 'Notificaciones','icon' => 'fa fa-cog','path' => 'notifications','table_name' => 'cms_notifications','controller' => 'NotificationsController','is_protected' => '1','is_active' => '1','created_at' => '2017-10-07 20:10:06','updated_at' => NULL,'deleted_at' => NULL),
  array('id' => '2','name' => 'Privilegios','icon' => 'fa fa-cog','path' => 'privileges','table_name' => 'cms_privileges','controller' => 'PrivilegesController','is_protected' => '1','is_active' => '1','created_at' => '2017-10-07 20:10:06','updated_at' => NULL,'deleted_at' => NULL),
  array('id' => '3','name' => 'Privilegios & Roles','icon' => 'fa fa-cog','path' => 'privileges_roles','table_name' => 'cms_privileges_roles','controller' => 'PrivilegesRolesController','is_protected' => '1','is_active' => '1','created_at' => '2017-10-07 20:10:06','updated_at' => NULL,'deleted_at' => NULL),
  array('id' => '4','name' => 'Gestión de usuarios','icon' => 'fa fa-users','path' => 'users','table_name' => 'cms_users','controller' => 'AdminCmsUsersController','is_protected' => '0','is_active' => '1','created_at' => '2017-10-07 20:10:06','updated_at' => NULL,'deleted_at' => NULL),
  array('id' => '5','name' => 'Ajustes','icon' => 'fa fa-cog','path' => 'settings','table_name' => 'cms_settings','controller' => 'SettingsController','is_protected' => '1','is_active' => '1','created_at' => '2017-10-07 20:10:06','updated_at' => NULL,'deleted_at' => NULL),
  array('id' => '6','name' => 'Generador de Módulos','icon' => 'fa fa-database','path' => 'module_generator','table_name' => 'cms_moduls','controller' => 'ModulsController','is_protected' => '1','is_active' => '1','created_at' => '2017-10-07 20:10:06','updated_at' => NULL,'deleted_at' => NULL),
  array('id' => '7','name' => 'Gestión de Menús','icon' => 'fa fa-bars','path' => 'menu_management','table_name' => 'cms_menus','controller' => 'MenusController','is_protected' => '1','is_active' => '1','created_at' => '2017-10-07 20:10:06','updated_at' => NULL,'deleted_at' => NULL),
  array('id' => '8','name' => 'Plantillas de Correo','icon' => 'fa fa-envelope-o','path' => 'email_templates','table_name' => 'cms_email_templates','controller' => 'EmailTemplatesController','is_protected' => '1','is_active' => '1','created_at' => '2017-10-07 20:10:06','updated_at' => NULL,'deleted_at' => NULL),
  array('id' => '9','name' => 'Generador de Estadísticas','icon' => 'fa fa-dashboard','path' => 'statistic_builder','table_name' => 'cms_statistics','controller' => 'StatisticBuilderController','is_protected' => '1','is_active' => '1','created_at' => '2017-10-07 20:10:06','updated_at' => NULL,'deleted_at' => NULL),
  array('id' => '10','name' => 'Generador de API','icon' => 'fa fa-cloud-download','path' => 'api_generator','table_name' => '','controller' => 'ApiCustomController','is_protected' => '1','is_active' => '1','created_at' => '2017-10-07 20:10:06','updated_at' => NULL,'deleted_at' => NULL),
  array('id' => '11','name' => 'Log de Accesos (Usuarios)','icon' => 'fa fa-flag-o','path' => 'logs','table_name' => 'cms_logs','controller' => 'LogsController','is_protected' => '1','is_active' => '1','created_at' => '2017-10-07 20:10:06','updated_at' => NULL,'deleted_at' => NULL),
  array('id' => '12','name' => 'Contactos','icon' => 'fa fa-users','path' => 'contactos','table_name' => 'contactos','controller' => 'AdminContactosController','is_protected' => '0','is_active' => '0','created_at' => '2017-10-07 20:10:37','updated_at' => NULL,'deleted_at' => NULL),
  array('id' => '13','name' => 'Artículos Categorías','icon' => 'fa fa-tag','path' => 'articulos_categorias','table_name' => 'articulos_categorias','controller' => 'AdminArticulosCategoriasController','is_protected' => '0','is_active' => '0','created_at' => '2017-10-09 18:54:21','updated_at' => NULL,'deleted_at' => NULL),
  array('id' => '14','name' => 'Artículos','icon' => 'fa fa-cubes','path' => 'articulos','table_name' => 'articulos','controller' => 'AdminArticulosController','is_protected' => '0','is_active' => '0','created_at' => '2017-10-09 18:55:27','updated_at' => NULL,'deleted_at' => NULL),
  array('id' => '15','name' => 'Ordenes','icon' => 'fa fa-opencart','path' => 'orders','table_name' => 'orders','controller' => 'AdminOrdersController','is_protected' => '0','is_active' => '0','created_at' => '2017-10-09 23:48:23','updated_at' => NULL,'deleted_at' => NULL),
  array('id' => '16','name' => 'Inventario','icon' => 'fa fa-cube','path' => 'inventario','table_name' => 'inventario','controller' => 'AdminInventarioController','is_protected' => '0','is_active' => '0','created_at' => '2017-10-14 14:50:17','updated_at' => NULL,'deleted_at' => NULL),
  array('id' => '17','name' => 'Movimientos de inventario','icon' => 'fa fa-arrow-circle-o-right','path' => 'movimientos_detail','table_name' => 'movimientos_detail','controller' => 'AdminMovimientosDetailController','is_protected' => '0','is_active' => '0','created_at' => '2017-10-14 15:15:38','updated_at' => NULL,'deleted_at' => '2017-10-14 15:30:07'),
  array('id' => '18','name' => 'Movimientos','icon' => 'fa fa-repeat','path' => 'movimientos_detail18','table_name' => 'movimientos_detail','controller' => 'AdminMovimientosDetail18Controller','is_protected' => '0','is_active' => '0','created_at' => '2017-10-14 15:30:37','updated_at' => NULL,'deleted_at' => NULL),
  array('id' => '19','name' => 'Caja y Bancos','icon' => 'fa fa-bank','path' => 'cuentas_categorias','table_name' => 'cuentas_categorias','controller' => 'AdminCuentasCategoriasController','is_protected' => '0','is_active' => '0','created_at' => '2017-10-16 14:00:17','updated_at' => NULL,'deleted_at' => NULL),
  array('id' => '20','name' => 'Movimientos C&B','icon' => 'fa fa-refresh','path' => 'movimientos_cuentas_detail','table_name' => 'movimientos_cuentas_detail','controller' => 'AdminMovimientosCuentasDetailController','is_protected' => '0','is_active' => '0','created_at' => '2017-10-16 14:04:00','updated_at' => NULL,'deleted_at' => NULL),
  array('id' => '21','name' => 'Cuentas','icon' => 'fa fa-bank','path' => 'cuentas','table_name' => 'cuentas','controller' => 'AdminCuentasController','is_protected' => '0','is_active' => '0','created_at' => '2017-10-16 15:26:46','updated_at' => NULL,'deleted_at' => NULL),
  array('id' => '22','name' => 'Clientes','icon' => 'fa fa-users','path' => 'clientes','table_name' => 'contactos','controller' => 'AdminClientesController','is_protected' => '0','is_active' => '0','created_at' => '2017-10-29 23:08:25','updated_at' => NULL,'deleted_at' => NULL),
  array('id' => '23','name' => 'Proveedores','icon' => 'fa fa-users','path' => 'proveedores','table_name' => 'contactos','controller' => 'AdminProveedoresController','is_protected' => '0','is_active' => '0','created_at' => '2017-10-29 23:11:07','updated_at' => NULL,'deleted_at' => NULL),
  array('id' => '24','name' => 'Vendedor','icon' => 'fa fa-suitcase','path' => 'vendedor','table_name' => 'vendedor','controller' => 'AdminVendedorController','is_protected' => '0','is_active' => '0','created_at' => '2017-10-30 11:09:45','updated_at' => NULL,'deleted_at' => NULL),
  array('id' => '25','name' => 'Facturas','icon' => 'fa fa-shopping-cart','path' => 'facturas','table_name' => 'orders_fv','controller' => 'AdminFacturasController','is_protected' => '0','is_active' => '0','created_at' => '2017-10-30 13:54:23','updated_at' => NULL,'deleted_at' => NULL),
  array('id' => '26','name' => 'Cotizaciones','icon' => 'fa fa-plus','path' => 'orders_cotizacion','table_name' => 'orders_cotizacion','controller' => 'AdminOrdersCotizacionController','is_protected' => '0','is_active' => '0','created_at' => '2017-12-10 15:44:24','updated_at' => NULL,'deleted_at' => NULL),
  array('id' => '27','name' => 'Compra','icon' => 'fa fa-shopping-cart','path' => 'orders_compra','table_name' => 'orders_compra','controller' => 'AdminOrdersCompraController','is_protected' => '0','is_active' => '0','created_at' => '2017-12-10 16:51:10','updated_at' => NULL,'deleted_at' => NULL)
);

/* `gealight`.`cms_notifications` */
$cms_notifications = array(
);

/* `gealight`.`cms_privileges` */
$cms_privileges = array(
  array('id' => '1','name' => 'Super Administrator','is_superadmin' => '1','theme_color' => 'skin-blue','created_at' => '2017-10-10 04:33:30','updated_at' => NULL),
  array('id' => '2','name' => 'Demo','is_superadmin' => '0','theme_color' => 'skin-blue','created_at' => NULL,'updated_at' => NULL)
);

/* `gealight`.`cms_privileges_roles` */
$cms_privileges_roles = array(
  array('id' => '1','is_visible' => '1','is_create' => '0','is_read' => '0','is_edit' => '0','is_delete' => '0','id_cms_privileges' => '1','id_cms_moduls' => '1','created_at' => '2017-10-08 00:40:06','updated_at' => NULL),
  array('id' => '2','is_visible' => '1','is_create' => '1','is_read' => '1','is_edit' => '1','is_delete' => '1','id_cms_privileges' => '1','id_cms_moduls' => '2','created_at' => '2017-10-08 00:40:06','updated_at' => NULL),
  array('id' => '3','is_visible' => '0','is_create' => '1','is_read' => '1','is_edit' => '1','is_delete' => '1','id_cms_privileges' => '1','id_cms_moduls' => '3','created_at' => '2017-10-08 00:40:06','updated_at' => NULL),
  array('id' => '4','is_visible' => '1','is_create' => '1','is_read' => '1','is_edit' => '1','is_delete' => '1','id_cms_privileges' => '1','id_cms_moduls' => '4','created_at' => '2017-10-08 00:40:06','updated_at' => NULL),
  array('id' => '5','is_visible' => '1','is_create' => '1','is_read' => '1','is_edit' => '1','is_delete' => '1','id_cms_privileges' => '1','id_cms_moduls' => '5','created_at' => '2017-10-08 00:40:06','updated_at' => NULL),
  array('id' => '6','is_visible' => '1','is_create' => '1','is_read' => '1','is_edit' => '1','is_delete' => '1','id_cms_privileges' => '1','id_cms_moduls' => '6','created_at' => '2017-10-08 00:40:06','updated_at' => NULL),
  array('id' => '7','is_visible' => '1','is_create' => '1','is_read' => '1','is_edit' => '1','is_delete' => '1','id_cms_privileges' => '1','id_cms_moduls' => '7','created_at' => '2017-10-08 00:40:06','updated_at' => NULL),
  array('id' => '8','is_visible' => '1','is_create' => '1','is_read' => '1','is_edit' => '1','is_delete' => '1','id_cms_privileges' => '1','id_cms_moduls' => '8','created_at' => '2017-10-08 00:40:06','updated_at' => NULL),
  array('id' => '9','is_visible' => '1','is_create' => '1','is_read' => '1','is_edit' => '1','is_delete' => '1','id_cms_privileges' => '1','id_cms_moduls' => '9','created_at' => '2017-10-08 00:40:06','updated_at' => NULL),
  array('id' => '10','is_visible' => '1','is_create' => '1','is_read' => '1','is_edit' => '1','is_delete' => '1','id_cms_privileges' => '1','id_cms_moduls' => '10','created_at' => '2017-10-08 00:40:06','updated_at' => NULL),
  array('id' => '11','is_visible' => '1','is_create' => '0','is_read' => '1','is_edit' => '0','is_delete' => '1','id_cms_privileges' => '1','id_cms_moduls' => '11','created_at' => '2017-10-08 00:40:06','updated_at' => NULL),
  array('id' => '12','is_visible' => '1','is_create' => '1','is_read' => '1','is_edit' => '1','is_delete' => '1','id_cms_privileges' => '1','id_cms_moduls' => '12','created_at' => NULL,'updated_at' => NULL),
  array('id' => '13','is_visible' => '1','is_create' => '1','is_read' => '1','is_edit' => '1','is_delete' => '1','id_cms_privileges' => '1','id_cms_moduls' => '13','created_at' => NULL,'updated_at' => NULL),
  array('id' => '14','is_visible' => '1','is_create' => '1','is_read' => '1','is_edit' => '1','is_delete' => '1','id_cms_privileges' => '1','id_cms_moduls' => '14','created_at' => NULL,'updated_at' => NULL),
  array('id' => '15','is_visible' => '1','is_create' => '1','is_read' => '1','is_edit' => '1','is_delete' => '1','id_cms_privileges' => '1','id_cms_moduls' => '15','created_at' => NULL,'updated_at' => NULL),
  array('id' => '16','is_visible' => '1','is_create' => '1','is_read' => '1','is_edit' => '1','is_delete' => '0','id_cms_privileges' => '2','id_cms_moduls' => '14','created_at' => NULL,'updated_at' => NULL),
  array('id' => '17','is_visible' => '1','is_create' => '1','is_read' => '1','is_edit' => '1','is_delete' => '0','id_cms_privileges' => '2','id_cms_moduls' => '13','created_at' => NULL,'updated_at' => NULL),
  array('id' => '18','is_visible' => '1','is_create' => '1','is_read' => '1','is_edit' => '1','is_delete' => '0','id_cms_privileges' => '2','id_cms_moduls' => '12','created_at' => NULL,'updated_at' => NULL),
  array('id' => '19','is_visible' => '1','is_create' => '0','is_read' => '1','is_edit' => '0','is_delete' => '0','id_cms_privileges' => '2','id_cms_moduls' => '4','created_at' => NULL,'updated_at' => NULL),
  array('id' => '20','is_visible' => '1','is_create' => '0','is_read' => '1','is_edit' => '0','is_delete' => '0','id_cms_privileges' => '2','id_cms_moduls' => '15','created_at' => NULL,'updated_at' => NULL),
  array('id' => '21','is_visible' => '1','is_create' => '0','is_read' => '1','is_edit' => '0','is_delete' => '0','id_cms_privileges' => '2','id_cms_moduls' => '16','created_at' => NULL,'updated_at' => NULL),
  array('id' => '22','is_visible' => '1','is_create' => '0','is_read' => '1','is_edit' => '0','is_delete' => '0','id_cms_privileges' => '2','id_cms_moduls' => '18','created_at' => NULL,'updated_at' => NULL),
  array('id' => '23','is_visible' => '1','is_create' => '0','is_read' => '1','is_edit' => '0','is_delete' => '0','id_cms_privileges' => '2','id_cms_moduls' => '17','created_at' => NULL,'updated_at' => NULL),
  array('id' => '24','is_visible' => '1','is_create' => '1','is_read' => '1','is_edit' => '1','is_delete' => '1','id_cms_privileges' => '1','id_cms_moduls' => '19','created_at' => NULL,'updated_at' => NULL),
  array('id' => '25','is_visible' => '1','is_create' => '1','is_read' => '1','is_edit' => '1','is_delete' => '1','id_cms_privileges' => '1','id_cms_moduls' => '20','created_at' => NULL,'updated_at' => NULL),
  array('id' => '26','is_visible' => '1','is_create' => '1','is_read' => '1','is_edit' => '1','is_delete' => '1','id_cms_privileges' => '1','id_cms_moduls' => '21','created_at' => NULL,'updated_at' => NULL),
  array('id' => '27','is_visible' => '1','is_create' => '1','is_read' => '1','is_edit' => '1','is_delete' => '0','id_cms_privileges' => '2','id_cms_moduls' => '19','created_at' => NULL,'updated_at' => NULL),
  array('id' => '28','is_visible' => '1','is_create' => '0','is_read' => '1','is_edit' => '0','is_delete' => '0','id_cms_privileges' => '2','id_cms_moduls' => '21','created_at' => NULL,'updated_at' => NULL),
  array('id' => '29','is_visible' => '1','is_create' => '0','is_read' => '1','is_edit' => '0','is_delete' => '0','id_cms_privileges' => '2','id_cms_moduls' => '20','created_at' => NULL,'updated_at' => NULL),
  array('id' => '30','is_visible' => '1','is_create' => '1','is_read' => '1','is_edit' => '1','is_delete' => '1','id_cms_privileges' => '1','id_cms_moduls' => '22','created_at' => NULL,'updated_at' => NULL),
  array('id' => '31','is_visible' => '1','is_create' => '1','is_read' => '1','is_edit' => '1','is_delete' => '1','id_cms_privileges' => '1','id_cms_moduls' => '23','created_at' => NULL,'updated_at' => NULL),
  array('id' => '32','is_visible' => '1','is_create' => '1','is_read' => '1','is_edit' => '1','is_delete' => '0','id_cms_privileges' => '2','id_cms_moduls' => '22','created_at' => NULL,'updated_at' => NULL),
  array('id' => '33','is_visible' => '1','is_create' => '1','is_read' => '1','is_edit' => '1','is_delete' => '0','id_cms_privileges' => '2','id_cms_moduls' => '23','created_at' => NULL,'updated_at' => NULL),
  array('id' => '34','is_visible' => '1','is_create' => '1','is_read' => '1','is_edit' => '1','is_delete' => '1','id_cms_privileges' => '1','id_cms_moduls' => '24','created_at' => NULL,'updated_at' => NULL),
  array('id' => '35','is_visible' => '1','is_create' => '1','is_read' => '1','is_edit' => '1','is_delete' => '1','id_cms_privileges' => '1','id_cms_moduls' => '25','created_at' => NULL,'updated_at' => NULL),
  array('id' => '36','is_visible' => '1','is_create' => '0','is_read' => '1','is_edit' => '0','is_delete' => '0','id_cms_privileges' => '2','id_cms_moduls' => '25','created_at' => NULL,'updated_at' => NULL),
  array('id' => '37','is_visible' => '1','is_create' => '1','is_read' => '1','is_edit' => '1','is_delete' => '0','id_cms_privileges' => '2','id_cms_moduls' => '24','created_at' => NULL,'updated_at' => NULL),
  array('id' => '38','is_visible' => '1','is_create' => '1','is_read' => '1','is_edit' => '1','is_delete' => '1','id_cms_privileges' => '1','id_cms_moduls' => '26','created_at' => NULL,'updated_at' => NULL),
  array('id' => '39','is_visible' => '1','is_create' => '1','is_read' => '1','is_edit' => '1','is_delete' => '1','id_cms_privileges' => '1','id_cms_moduls' => '27','created_at' => NULL,'updated_at' => NULL),
  array('id' => '40','is_visible' => '1','is_create' => '0','is_read' => '1','is_edit' => '0','is_delete' => '0','id_cms_privileges' => '2','id_cms_moduls' => '27','created_at' => NULL,'updated_at' => NULL),
  array('id' => '41','is_visible' => '1','is_create' => '0','is_read' => '1','is_edit' => '0','is_delete' => '0','id_cms_privileges' => '2','id_cms_moduls' => '26','created_at' => NULL,'updated_at' => NULL)
);

/* `gealight`.`cms_settings` */
$cms_settings = array(
  array('id' => '1','name' => 'login_background_color','content' => '','content_input_type' => 'text','dataenum' => NULL,'helper' => 'Input hexacode','created_at' => '2017-10-07 19:22:58','updated_at' => NULL,'group_setting' => 'Login Register Style','label' => 'Login Background Color'),
  array('id' => '2','name' => 'login_font_color','content' => '','content_input_type' => 'text','dataenum' => NULL,'helper' => 'Input hexacode','created_at' => '2017-10-07 19:22:58','updated_at' => NULL,'group_setting' => 'Login Register Style','label' => 'Login Font Color'),
  array('id' => '3','name' => 'login_background_image','content' => 'uploads/2017-10/d8e46983563a8927b1a6e7f16646aa95.jpg','content_input_type' => 'upload_image','dataenum' => NULL,'helper' => NULL,'created_at' => '2017-10-07 19:22:58','updated_at' => NULL,'group_setting' => 'Login Register Style','label' => 'Login Background Image'),
  array('id' => '4','name' => 'email_sender','content' => 'support@crudbooster.com','content_input_type' => 'text','dataenum' => NULL,'helper' => NULL,'created_at' => '2017-10-07 19:22:58','updated_at' => NULL,'group_setting' => 'Email Setting','label' => 'Email Sender'),
  array('id' => '5','name' => 'smtp_driver','content' => 'mail','content_input_type' => 'select','dataenum' => 'smtp,mail,sendmail','helper' => NULL,'created_at' => '2017-10-07 19:22:58','updated_at' => NULL,'group_setting' => 'Email Setting','label' => 'Mail Driver'),
  array('id' => '6','name' => 'smtp_host','content' => '','content_input_type' => 'text','dataenum' => NULL,'helper' => NULL,'created_at' => '2017-10-07 19:22:58','updated_at' => NULL,'group_setting' => 'Email Setting','label' => 'SMTP Host'),
  array('id' => '7','name' => 'smtp_port','content' => '25','content_input_type' => 'text','dataenum' => NULL,'helper' => 'default 25','created_at' => '2017-10-07 19:22:58','updated_at' => NULL,'group_setting' => 'Email Setting','label' => 'SMTP Port'),
  array('id' => '8','name' => 'smtp_username','content' => '','content_input_type' => 'text','dataenum' => NULL,'helper' => NULL,'created_at' => '2017-10-07 19:22:58','updated_at' => NULL,'group_setting' => 'Email Setting','label' => 'SMTP Username'),
  array('id' => '9','name' => 'smtp_password','content' => '','content_input_type' => 'text','dataenum' => NULL,'helper' => NULL,'created_at' => '2017-10-07 19:22:58','updated_at' => NULL,'group_setting' => 'Email Setting','label' => 'SMTP Password'),
  array('id' => '10','name' => 'appname','content' => 'Gea-Light','content_input_type' => 'text','dataenum' => NULL,'helper' => NULL,'created_at' => '2017-10-07 19:22:58','updated_at' => NULL,'group_setting' => 'Application Setting','label' => 'Application Name'),
  array('id' => '11','name' => 'default_paper_size','content' => 'Legal','content_input_type' => 'text','dataenum' => NULL,'helper' => 'Paper size, ex : A4, Legal, etc','created_at' => '2017-10-07 19:22:58','updated_at' => NULL,'group_setting' => 'Application Setting','label' => 'Default Paper Print Size'),
  array('id' => '12','name' => 'logo','content' => 'uploads/2017-10/bacb435e092f597895651094daa56bc4.png','content_input_type' => 'upload_image','dataenum' => NULL,'helper' => NULL,'created_at' => '2017-10-07 19:22:58','updated_at' => NULL,'group_setting' => 'Application Setting','label' => 'Logo'),
  array('id' => '13','name' => 'favicon','content' => 'uploads/2017-10/5b9820ab4fc181a852824cd782efe8a1.png','content_input_type' => 'upload_image','dataenum' => NULL,'helper' => NULL,'created_at' => '2017-10-07 19:22:58','updated_at' => NULL,'group_setting' => 'Application Setting','label' => 'Favicon'),
  array('id' => '14','name' => 'api_debug_mode','content' => 'true','content_input_type' => 'select','dataenum' => 'true,false','helper' => NULL,'created_at' => '2017-10-07 19:22:58','updated_at' => NULL,'group_setting' => 'Application Setting','label' => 'API Debug Mode'),
  array('id' => '15','name' => 'google_api_key','content' => '','content_input_type' => 'text','dataenum' => NULL,'helper' => NULL,'created_at' => '2017-10-07 19:22:58','updated_at' => NULL,'group_setting' => 'Application Setting','label' => 'Google API Key'),
  array('id' => '16','name' => 'google_fcm_key','content' => '','content_input_type' => 'text','dataenum' => NULL,'helper' => NULL,'created_at' => '2017-10-07 19:22:58','updated_at' => NULL,'group_setting' => 'Application Setting','label' => 'Google FCM Key')
);

/* `gealight`.`cms_statistics` */
$cms_statistics = array(
  array('id' => '1','name' => 'Dashboard','slug' => 'dashboard','created_at' => '2017-03-17 19:53:45','updated_at' => NULL)
);

/* `gealight`.`cms_statistic_components` */
$cms_statistic_components = array(
  array('id' => '1','id_cms_statistics' => '1','componentID' => '17620f3b0259d8bb0527a828a46ce27b','component_name' => 'smallbox','area_name' => 'area1','sorting' => '0','name' => NULL,'config' => '{"name":"Total Factura de Venta","icon":"ion-bag","color":"bg-green","link":"#","sql":"select count(id) from `orders_fv` where documento=\'Factura de Venta\' and deleted_at is null"}','created_at' => '2017-03-17 19:53:55','updated_at' => NULL),
  array('id' => '2','id_cms_statistics' => '1','componentID' => 'dc9bf1879b58e69402c7c906ba4414b1','component_name' => 'smallbox','area_name' => 'area2','sorting' => '0','name' => NULL,'config' => '{"name":"Total Factura de Venta Cantidad","icon":"ion-cash","color":"bg-green","link":"#","sql":"select IFNULL(sum(grand_total),0) from `orders_fv` where documento=\'Factura de Venta\' and deleted_at is null"}','created_at' => '2017-03-17 19:55:54','updated_at' => NULL),
  array('id' => '3','id_cms_statistics' => '1','componentID' => 'ef86ce0883e86bdec1f49ead38443305','component_name' => 'smallbox','area_name' => 'area3','sorting' => '0','name' => NULL,'config' => '{"name":"Total Factura de Venta Actuales Cantidad","icon":"ion-cash","color":"bg-red","link":"#","sql":"select IFNULL(SUM(grand_total),0) from `orders_fv` where documento=\'Factura de Venta\' and DATE(created_at) = CURDATE()"}','created_at' => '2017-03-17 19:57:39','updated_at' => NULL),
  array('id' => '4','id_cms_statistics' => '1','componentID' => 'c5f1401d2c5f2528efacf5e2601481af','component_name' => 'smallbox','area_name' => 'area4','sorting' => '0','name' => NULL,'config' => '{"name":"Total Factura de Venta Actuales","icon":"ion-bag","color":"bg-red","link":"#","sql":"select count(id) from `orders_fv` where documento=\'Factura de Venta\' and DATE(created_at) = CURDATE()"}','created_at' => '2017-03-17 19:59:07','updated_at' => NULL),
  array('id' => '5','id_cms_statistics' => '1','componentID' => '6b8643480ea7e4763a8a9279496808a1','component_name' => 'chartline','area_name' => 'area5','sorting' => '0','name' => NULL,'config' => '{"name":"Factura de Venta de este a\\u00f1o","sql":"select date(created_at) as label, count(id) as value from `orders_fv` where documento=\'Factura de Venta\' and year(created_at) = YEAR(CURDATE()) group by label","area_name":"Total Order","goals":""}','created_at' => '2017-03-17 20:00:18','updated_at' => NULL)
);

/* `gealight`.`cms_users` */
$cms_users = array(
  array('id' => '1','name' => 'Super Admin','photo' => 'uploads/1/2017-10/images.jpg','email' => 'admin@crudbooster.com','password' => '$2y$10$BEJ4nVIhScimJ0Fybxl2ZucRSmAttP6iZ9/BytOg60kG3.chYzHPm','id_cms_privileges' => '1','created_at' => '2017-10-10 04:33:30','updated_at' => '2017-10-10 22:04:41','status' => 'Active'),
  array('id' => '2','name' => 'Super Admin','photo' => 'uploads/1/2017-10/images.jpg','email' => 'super@admin.com','password' => '$2y$10$dwoYC63zPnq9flhzdHRIUue7LAsLOWF3T/2PHhjdXrwh5UF5Q2Zj2','id_cms_privileges' => '1','created_at' => '2017-10-10 04:33:43','updated_at' => '2017-10-10 22:04:18','status' => 'Active'),
  array('id' => '3','name' => 'Demo','photo' => 'uploads/1/2017-10/images.jpg','email' => 'demo@admin.com','password' => '$2y$10$UUO7EQbWRiWlSwKSr.R2/OumNNxXzbkdPcfSIRR.tk3.uR5VgTzV.','id_cms_privileges' => '2','created_at' => '2017-10-10 17:57:45','updated_at' => '2017-10-10 22:04:32','status' => NULL)
);

/* `gealight`.`contactos` */
$contactos = array(
  array('id' => '1','tipo' => 'Cliente','documento' => 'J-40936247-7','name' => 'Dolcegrotta888 C.A','telefono' => '0212-6817608','email' => 'dolcegrotta888@gmail.com','direccion' => 'Av intercomunal del Valle, Calle Nº 10, Casa Nº 23, PB, Sector Los Jardines del Valle Caracas','contacto_nombre' => 'Norelis','contacto_telefono' => '0212-6817608','contacto_email' => 'dolcegrotta888@gmail.com','contacto_detalles' => 'bapowuv@gmail.com','opcional' => 'In minim maxime labore beatae ullamco','opcional1' => NULL,'opcional2' => NULL,'opcional3' => NULL,'deleted_at' => NULL,'created_at' => '2017-10-30 14:11:13','updated_at' => '2017-11-01 15:44:17'),
  array('id' => '2','tipo' => 'Proveedor','documento' => '23032986225','name' => 'Montana Kirk','telefono' => '+524-41-7360257','email' => 'kijizyz@gmail.com','direccion' => 'Facilis magni rem libero totam id similique','contacto_nombre' => 'Ex minima blanditiis excepteur earum numquam minus pariatur','contacto_telefono' => '+885-29-8354468','contacto_email' => 'fylej@yahoo.com','contacto_detalles' => 'byzoryke@gmail.com','opcional' => '2222222','opcional1' => NULL,'opcional2' => NULL,'opcional3' => NULL,'deleted_at' => NULL,'created_at' => '2017-11-04 12:33:26','updated_at' => NULL),
  array('id' => '3','tipo' => 'Cliente','documento' => '000234545','name' => 'Robert Padovani','telefono' => '58 212-361.26.68 ','email' => 'padovani@tecvemar.net','direccion' => 'Guatire','contacto_nombre' => 'Robert Padovani','contacto_telefono' => '58 212-361.26.68 ','contacto_email' => NULL,'contacto_detalles' => NULL,'opcional' => NULL,'opcional1' => NULL,'opcional2' => NULL,'opcional3' => NULL,'deleted_at' => NULL,'created_at' => '2017-11-06 15:42:24','updated_at' => NULL),
  array('id' => '4','tipo' => 'Cliente','documento' => '17168754','name' => 'Douglas Nieves','telefono' => '2123471001','email' => 'douglasjosenieves@gmail.com','direccion' => 'Caracas','contacto_nombre' => 'Douglas','contacto_telefono' => '2123471001','contacto_email' => 'douglasjosenieves@gmail.com','contacto_detalles' => 'Prueba','opcional' => 'Caracas','opcional1' => NULL,'opcional2' => NULL,'opcional3' => NULL,'deleted_at' => NULL,'created_at' => '2017-12-08 18:41:40','updated_at' => NULL),
  array('id' => '5','tipo' => 'Cliente','documento' => '171687548','name' => 'Douglas Nieves','telefono' => '2123471001','email' => 'douglasjosenieves2@gmail.com','direccion' => 'dqdqw','contacto_nombre' => 'Douglas Jose Nieves Bolivar','contacto_telefono' => '2123471001','contacto_email' => 'douglasjosenieves@gmail.com','contacto_detalles' => 'qwe','opcional' => NULL,'opcional1' => NULL,'opcional2' => NULL,'opcional3' => NULL,'deleted_at' => NULL,'created_at' => '2017-12-08 18:42:54','updated_at' => '2017-12-08 18:51:22'),
  array('id' => '6','tipo' => 'Cliente','documento' => '17770210','name' => 'Francys','telefono' => '04242143637','email' => 'xucewob@yahoo.com','direccion' => 'Ullam exercitation aut consequatur minus nisi et sint vel qui aut ullamco','contacto_nombre' => 'Francys Alarcon','contacto_telefono' => '0212-6817608','contacto_email' => 'dawagyha@yahoo.com','contacto_detalles' => 'Fugit in doloribus Nam modi labore sunt sapiente dolorem aut proident','opcional' => NULL,'opcional1' => NULL,'opcional2' => NULL,'opcional3' => NULL,'deleted_at' => NULL,'created_at' => '2017-12-09 15:01:55','updated_at' => NULL),
  array('id' => '7','tipo' => 'Proveedor','documento' => '1770211','name' => 'Daniel','telefono' => '04141331946','email' => 'junyzaru@hotmail.com','direccion' => 'Incidunt commodo qui mollitia excepteur ipsum exercitation voluptate at provident fuga Quibusdam velit qui est','contacto_nombre' => 'Douglas','contacto_telefono' => '2123471001','contacto_email' => 'dysamil@yahoo.com','contacto_detalles' => 'Est maxime est eum exercitationem deserunt aliquam','opcional' => NULL,'opcional1' => NULL,'opcional2' => NULL,'opcional3' => NULL,'deleted_at' => NULL,'created_at' => '2017-12-09 15:02:42','updated_at' => NULL),
  array('id' => '8','tipo' => 'Proveedor','documento' => '17702118','name' => 'Mia Mosley','telefono' => '04551654456','email' => 'lyhaf@yahoo.com','direccion' => 'Non dolore amet corporis sint ratione illo non aut velit numquam nisi alias','contacto_nombre' => 'Patricia','contacto_telefono' => '041413314454','contacto_email' => 'xupat@gmail.com','contacto_detalles' => 'Pariatur Perspiciatis aut ut id nostrum qui unde iste reiciendis do','opcional' => NULL,'opcional1' => NULL,'opcional2' => NULL,'opcional3' => NULL,'deleted_at' => NULL,'created_at' => '2017-12-09 15:06:09','updated_at' => NULL)
);

/* `gealight`.`cuentas` */
$cuentas = array(
);

/* `gealight`.`cuentas_categorias` */
$cuentas_categorias = array(
  array('id' => '1','tipo' => 'Caja','sku' => NULL,'name' => 'CajaDemo','description' => 'prueba','saldo' => '0','deleted_at' => NULL,'created_at' => '2017-10-31 00:40:26','updated_at' => NULL)
);

/* `gealight`.`cuentas_detail` */
$cuentas_detail = array(
);

/* `gealight`.`inventario` */
$inventario = array(
);

/* `gealight`.`inventario_detail` */
$inventario_detail = array(
);

/* `gealight`.`migrations` */
$migrations = array(
  array('id' => '1','migration' => '2016_08_07_145904_add_table_cms_apicustom','batch' => '1'),
  array('id' => '2','migration' => '2016_08_07_150834_add_table_cms_dashboard','batch' => '1'),
  array('id' => '3','migration' => '2016_08_07_151210_add_table_cms_logs','batch' => '1'),
  array('id' => '4','migration' => '2016_08_07_151211_add_details_cms_logs','batch' => '1'),
  array('id' => '5','migration' => '2016_08_07_152014_add_table_cms_privileges','batch' => '1'),
  array('id' => '6','migration' => '2016_08_07_152214_add_table_cms_privileges_roles','batch' => '1'),
  array('id' => '7','migration' => '2016_08_07_152320_add_table_cms_settings','batch' => '1'),
  array('id' => '8','migration' => '2016_08_07_152421_add_table_cms_users','batch' => '1'),
  array('id' => '9','migration' => '2016_08_07_154624_add_table_cms_menus_privileges','batch' => '1'),
  array('id' => '10','migration' => '2016_08_07_154624_add_table_cms_moduls','batch' => '1'),
  array('id' => '11','migration' => '2016_08_17_225409_add_status_cms_users','batch' => '1'),
  array('id' => '12','migration' => '2016_08_20_125418_add_table_cms_notifications','batch' => '1'),
  array('id' => '13','migration' => '2016_09_04_033706_add_table_cms_email_queues','batch' => '1'),
  array('id' => '14','migration' => '2016_09_16_035347_add_group_setting','batch' => '1'),
  array('id' => '15','migration' => '2016_09_16_045425_add_label_setting','batch' => '1'),
  array('id' => '16','migration' => '2016_09_17_104728_create_nullable_cms_apicustom','batch' => '1'),
  array('id' => '17','migration' => '2016_10_01_141740_add_method_type_apicustom','batch' => '1'),
  array('id' => '18','migration' => '2016_10_01_141846_add_parameters_apicustom','batch' => '1'),
  array('id' => '19','migration' => '2016_10_01_141934_add_responses_apicustom','batch' => '1'),
  array('id' => '20','migration' => '2016_10_01_144826_add_table_apikey','batch' => '1'),
  array('id' => '21','migration' => '2016_11_14_141657_create_cms_menus','batch' => '1'),
  array('id' => '22','migration' => '2016_11_15_132350_create_cms_email_templates','batch' => '1'),
  array('id' => '23','migration' => '2016_11_15_190410_create_cms_statistics','batch' => '1'),
  array('id' => '24','migration' => '2016_11_17_102740_create_cms_statistic_components','batch' => '1'),
  array('id' => '25','migration' => '2017_03_17_085044_create_orders','batch' => '1'),
  array('id' => '26','migration' => '2017_03_17_085104_create_orders_detail','batch' => '1'),
  array('id' => '27','migration' => '2017_06_06_164501_add_deleted_at_cms_moduls','batch' => '1'),
  array('id' => '28','migration' => '2017_10_07_183837_create_contactos','batch' => '1'),
  array('id' => '29','migration' => '2017_10_09_183423_create_articulos','batch' => '1'),
  array('id' => '30','migration' => '2017_10_09_183921_create_articulos_categorias','batch' => '1'),
  array('id' => '31','migration' => '2017_10_14_085044_create_inventario','batch' => '1'),
  array('id' => '32','migration' => '2017_10_14_085104_create_inventario_detail','batch' => '1'),
  array('id' => '33','migration' => '2017_10_14_085105_create_movimientos_detail','batch' => '1'),
  array('id' => '34','migration' => '2017_10_16_085044_create_cuentas','batch' => '1'),
  array('id' => '35','migration' => '2017_10_16_085104_create_cuentas_detail','batch' => '1'),
  array('id' => '36','migration' => '2017_10_16_085105_create_movimientos_cuentas_detail','batch' => '1'),
  array('id' => '37','migration' => '2017_10_16_183921_create_cuentas_categorias','batch' => '1'),
  array('id' => '38','migration' => '2017_10_30_085044_create_orders_compra','batch' => '1'),
  array('id' => '39','migration' => '2017_10_30_085044_create_orders_cotizacion','batch' => '1'),
  array('id' => '40','migration' => '2017_10_30_085044_create_orders_fv','batch' => '1'),
  array('id' => '41','migration' => '2017_10_30_085104_create_orders_detail_compra','batch' => '1'),
  array('id' => '42','migration' => '2017_10_30_085104_create_orders_detail_cotizacion','batch' => '1'),
  array('id' => '43','migration' => '2017_10_30_183921_create_vendedor','batch' => '1')
);

/* `gealight`.`movimientos_cuentas_detail` */
$movimientos_cuentas_detail = array(
);

/* `gealight`.`movimientos_detail` */
$movimientos_detail = array(
  array('id' => '1','deleted_at' => '2017-12-10 17:28:02','items_orders_id' => '00001','items_orders_doc' => 'Compra','items_id' => '1','items_sku' => '171354949542','name' => 'Computadora','items_tax' => '0','precio' => '5','cantidad' => '10','total_mas_tax' => '0','total' => '50','enviado' => '0'),
  array('id' => '2','deleted_at' => NULL,'items_orders_id' => '00002','items_orders_doc' => 'Compra','items_id' => '1','items_sku' => '171354949542','name' => 'Computadora','items_tax' => '0','precio' => '5','cantidad' => '10','total_mas_tax' => '0','total' => '50','enviado' => '0'),
  array('id' => '3','deleted_at' => NULL,'items_orders_id' => '00001','items_orders_doc' => 'Factura de Venta','items_id' => '1','items_sku' => '171354949542','name' => 'Computadora','items_tax' => '0','precio' => '45','cantidad' => '-2','total_mas_tax' => '0','total' => '90','enviado' => '0'),
  array('id' => '4','deleted_at' => NULL,'items_orders_id' => '00001','items_orders_doc' => 'Factura de Venta','items_id' => '1','items_sku' => '171354949542','name' => 'Computadora','items_tax' => '0','precio' => '45','cantidad' => '-2','total_mas_tax' => '0','total' => '90','enviado' => '0')
);

/* `gealight`.`orders` */
$orders = array(
);

/* `gealight`.`orders_compra` */
$orders_compra = array(
  array('id' => '1','created_at' => '2017-12-10 17:03:23','updated_at' => NULL,'deleted_at' => NULL,'contactos_id' => '4','vendedor_id' => '1','tax' => '12','moneda' => 'Bs','documento' => 'Compra','order_numero' => '00001','numero_de_control' => NULL,'sub_total' => '50','tax_total' => '6','detalle' => '[{"items_orders_id":"","items_orders_doc":"Compra","items_id":1,"items_sku":"171354949542","items_tax":12,"name":"Computadora","precio":5,"cantidad":"10","total":50}]','grand_total' => '56','saldo' => '56','enviado' => '0','vence' => '2017-12-10','opcional' => '1543456','opcional1' => '1234564','opcional2' => NULL,'opcional3' => NULL,'es_activo' => '1'),
  array('id' => '2','created_at' => '2017-12-10 17:39:10','updated_at' => NULL,'deleted_at' => NULL,'contactos_id' => '4','vendedor_id' => '1','tax' => '12','moneda' => 'Bs','documento' => 'Compra','order_numero' => '00002','numero_de_control' => NULL,'sub_total' => '50','tax_total' => '6','detalle' => '[{"items_orders_id":"","items_orders_doc":"Compra","items_id":1,"items_sku":"171354949542","items_tax":12,"name":"Computadora","precio":5,"cantidad":"10","total":50}]','grand_total' => '56','saldo' => '56','enviado' => '0','vence' => '2017-12-10','opcional' => 'COMPRA DE FIN DE AÑO','opcional1' => '123456','opcional2' => NULL,'opcional3' => NULL,'es_activo' => '0')
);

/* `gealight`.`orders_cotizacion` */
$orders_cotizacion = array(
  array('id' => '1','created_at' => '2017-12-10 16:20:12','updated_at' => NULL,'deleted_at' => '2017-12-10 16:26:12','contactos_id' => '4','vendedor_id' => '1','tax' => '12','moneda' => 'Bs','documento' => 'Cotización','order_numero' => '00001','numero_de_control' => NULL,'sub_total' => '45','tax_total' => '5.4','detalle' => '[{"items_orders_id":"","items_orders_doc":"Cotización","items_id":1,"items_sku":"171354949542","items_tax":12,"name":"Computadora","precio":45,"cantidad":"1","total":45}]','grand_total' => '50.4','saldo' => '50.4','enviado' => '0','vence' => '2017-12-10','opcional' => '154564','opcional1' => '1234654','opcional2' => NULL,'opcional3' => NULL,'es_activo' => '0'),
  array('id' => '2','created_at' => '2017-12-10 16:26:46','updated_at' => NULL,'deleted_at' => NULL,'contactos_id' => '4','vendedor_id' => '1','tax' => '12','moneda' => 'Bs','documento' => 'Cotización','order_numero' => '00002','numero_de_control' => NULL,'sub_total' => '45','tax_total' => '5.4','detalle' => '[{"items_orders_id":"","items_orders_doc":"Cotización","items_id":1,"items_sku":"171354949542","items_tax":12,"name":"Computadora","precio":45,"cantidad":"1","total":45}]','grand_total' => '50.4','saldo' => '50.4','enviado' => '0','vence' => '2017-12-10','opcional' => 'prueba','opcional1' => '123456','opcional2' => NULL,'opcional3' => NULL,'es_activo' => '0'),
  array('id' => '3','created_at' => '2017-12-10 17:35:29','updated_at' => NULL,'deleted_at' => NULL,'contactos_id' => '4','vendedor_id' => '1','tax' => '12','moneda' => 'Bs','documento' => 'Cotización','order_numero' => '00003','numero_de_control' => NULL,'sub_total' => '90','tax_total' => '10.8','detalle' => '[{"items_orders_id":"","items_orders_doc":"Cotización","items_id":1,"items_sku":"171354949542","items_tax":12,"name":"Computadora","precio":45,"cantidad":"2","total":90}]','grand_total' => '100.8','saldo' => '100.8','enviado' => '0','vence' => '2017-12-10','opcional' => 'condiciones 2x1','opcional1' => '123456','opcional2' => NULL,'opcional3' => NULL,'es_activo' => '0')
);

/* `gealight`.`orders_detail` */
$orders_detail = array(
);

/* `gealight`.`orders_detail_compra` */
$orders_detail_compra = array(
  array('id' => '1','deleted_at' => NULL,'items_orders_id' => '','items_orders_doc' => 'Compra','items_id' => '1','items_sku' => '171354949542','name' => 'Computadora','items_tax' => '12','precio' => '5','cantidad' => '10','total' => '50','total_mas_tax' => '0','enviado' => '0','es_activo' => '0'),
  array('id' => '2','deleted_at' => NULL,'items_orders_id' => '','items_orders_doc' => 'Compra','items_id' => '1','items_sku' => '171354949542','name' => 'Computadora','items_tax' => '12','precio' => '5','cantidad' => '10','total' => '50','total_mas_tax' => '0','enviado' => '0','es_activo' => '0')
);

/* `gealight`.`orders_detail_cotizacion` */
$orders_detail_cotizacion = array(
  array('id' => '1','deleted_at' => NULL,'items_orders_id' => '','items_orders_doc' => 'Cotización','items_id' => '1','items_sku' => '171354949542','name' => 'Computadora','items_tax' => '12','precio' => '45','cantidad' => '1','total' => '45','total_mas_tax' => '0','enviado' => '0','es_activo' => '0'),
  array('id' => '2','deleted_at' => NULL,'items_orders_id' => '','items_orders_doc' => 'Cotización','items_id' => '1','items_sku' => '171354949542','name' => 'Computadora','items_tax' => '12','precio' => '45','cantidad' => '1','total' => '45','total_mas_tax' => '0','enviado' => '0','es_activo' => '0'),
  array('id' => '3','deleted_at' => NULL,'items_orders_id' => '','items_orders_doc' => 'Cotización','items_id' => '1','items_sku' => '171354949542','name' => 'Computadora','items_tax' => '12','precio' => '45','cantidad' => '1','total' => '45','total_mas_tax' => '0','enviado' => '0','es_activo' => '0'),
  array('id' => '4','deleted_at' => NULL,'items_orders_id' => '','items_orders_doc' => 'Cotización','items_id' => '1','items_sku' => '171354949542','name' => 'Computadora','items_tax' => '12','precio' => '45','cantidad' => '1','total' => '45','total_mas_tax' => '0','enviado' => '0','es_activo' => '0'),
  array('id' => '5','deleted_at' => NULL,'items_orders_id' => '','items_orders_doc' => 'Cotización','items_id' => '1','items_sku' => '171354949542','name' => 'Computadora','items_tax' => '12','precio' => '45','cantidad' => '2','total' => '90','total_mas_tax' => '0','enviado' => '0','es_activo' => '0')
);

/* `gealight`.`orders_fv` */
$orders_fv = array(
);

/* `gealight`.`vendedor` */
$vendedor = array(
  array('id' => '1','name' => 'VenDemo','comision' => '0','opcional' => NULL,'opcional1' => NULL,'opcional2' => NULL,'opcional3' => NULL,'deleted_at' => NULL,'created_at' => '2017-10-30 14:09:33','updated_at' => NULL)
);





DB::table('cms_menus')->delete($cms_menus);
DB::table('cms_menus')->insert($cms_menus);

DB::table('cms_menus_privileges')->delete($cms_menus_privileges);
DB::table('cms_menus_privileges')->insert($cms_menus_privileges);


DB::table('cms_moduls')->delete($cms_moduls);
DB::table('cms_moduls')->insert($cms_moduls);


DB::table('cms_privileges')->delete($cms_privileges);
DB::table('cms_privileges')->insert($cms_privileges);


DB::table('cms_privileges_roles')->delete($cms_privileges_roles);
DB::table('cms_privileges_roles')->insert($cms_privileges_roles);


DB::table('cms_settings')->delete($cms_settings);
DB::table('cms_settings')->insert($cms_settings);


DB::table('cms_statistics')->delete($cms_statistics);
DB::table('cms_statistics')->insert($cms_statistics); 

DB::table('cms_statistic_components')->delete($cms_statistic_components);
DB::table('cms_statistic_components')->insert($cms_statistic_components);

DB::table('cuentas_categorias')->delete($cuentas_categorias);
DB::table('cuentas_categorias')->insert($cuentas_categorias);

DB::table('cms_users')->delete($cms_users);
DB::table('cms_users')->insert($cms_users);

DB::table('vendedor')->delete($vendedor);
DB::table('vendedor')->insert($vendedor);

 DB::table('articulos_categorias')->delete($articulos_categorias);
DB::table('articulos_categorias')->insert($articulos_categorias);


 DB::table('articulos')->delete($articulos);
DB::table('articulos')->insert($articulos);

 DB::table('contactos')->delete($contactos);
DB::table('contactos')->insert($contactos);

}



}

