<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contactos', function (Blueprint $table) {
         
            $table->increments('id');
            $table->string('tipo')->nullable();
            $table->string('documento')->nullable();
            $table->string('name')->nullable();
            $table->string('telefono')->nullable();
            $table->string('email')->nullable();
            $table->text('direccion')->nullable();
              $table->string('contacto_nombre')->nullable();
              $table->string('contacto_telefono')->nullable();
              $table->string('contacto_email')->nullable();
            $table->text('contacto_detalles')->nullable();
            $table->text('opcional')->nullable();
            $table->text('opcional1')->nullable(); 
            $table->text('opcional2')->nullable(); 
            $table->text('opcional3')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contactos');
    }
}
