<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventarioDetail extends Migration
{
    /**
     * Run the migrations. "items_orders_id": 17101303288, "items_orders_doc": "Factura", "items_id": 8, "items_sku": "488258750995", "name": "Harina Pan", "precio": 18, "cantidad": "34", "total": 612

         [items_orders_id] => 17101304590
            [items_orders_doc] => Factura
            [items_id] => 1
            [items_sku] => 484401327301
            [name] => Pan
            [precio] => 61
            [cantidad] => 34
            [total] => 2074


     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventario_detail', function (Blueprint $table) {
                $table->increments('id');
              
                $table->string('items_orders_id')->nullable();
                $table->string('items_orders_doc',25)->nullable();   
                $table->string('items_id')->nullable();
                $table->string('items_sku')->nullable();
                $table->string('name')->nullable();   
                $table->double('precio')->default(0);
                $table->double('cantidad')->default(0);
                $table->double('total')->default(0);
                $table->double('enviado')->default(0);
                $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventario_detail');
    }
}
