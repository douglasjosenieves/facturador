<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->softDeletes();
            $table->integer('contactos_id')->nullable();
            $table->integer('vendedor_id')->nullable();;
            $table->string('tax',25)->nullable(); 
            $table->string('moneda',25)->nullable(); 

            $table->string('documento',25)->nullable(); 
            $table->string('order_numero',25)->nullable(); 
            $table->string('numero_de_control',25)->nullable();
            

            $table->double('sub_total')->default(0);
            $table->double('tax_total')->default(0);
            $table->text('detalle')->nullable(); 
             
            $table->double('grand_total')->default(0);
            $table->double('saldo')->default(0);
            $table->double('enviado')->default(0);
            $table->date('vence')->nullable();
             $table->text('opcional')->nullable();
              $table->text('opcional1')->nullable(); 
               $table->text('opcional2')->nullable(); 
               $table->text('opcional3')->nullable(); 
                $table->boolean('es_activo')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
