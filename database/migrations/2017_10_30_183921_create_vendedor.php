<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendedor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
 public function up()
    {
        Schema::create('vendedor', function (Blueprint $table) {
         
            $table->increments('id');
    
            $table->string('name')->nullable();
             $table->string('comision')->nullable();
               $table->text('opcional')->nullable();
              $table->text('opcional1')->nullable(); 
               $table->text('opcional2')->nullable(); 
               $table->text('opcional3')->nullable(); 
         
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendedor');
    }
}