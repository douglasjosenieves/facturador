<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticulos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articulos', function (Blueprint $table) {
         
            $table->increments('id');
            $table->string('sku')->nullable();
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->double('precio')->nullable();
            $table->double('precio_compra')->nullable();
            $table->double('stock')->default(0);
            $table->double('tax')->default(0);
           
            $table->text('unidad_de_medida')->nullable();
            $table->integer('articulos_categorias_id')->nullable(); 
                $table->string('photo')->nullable();
                 $table->string('photo1')->nullable();
                      
             $table->text('opcional')->nullable();
              $table->text('opcional1')->nullable(); 
               $table->text('opcional2')->nullable(); 
               $table->text('opcional3')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articulos');
    }
}