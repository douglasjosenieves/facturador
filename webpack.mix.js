const { mix } = require('laravel-mix');

 
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/factura-ventas.js', 'public/js')
mix.js('resources/assets/js/compras.js', 'public/js')

mix.js('resources/assets/js/cotizacion.js', 'public/js')
.mix.js('resources/assets/js/app-inventario.js', 'public/js')
.mix.js('resources/assets/js/app-cuentas.js', 'public/js')
 .sass('resources/assets/sass/app.scss', 'public/css')
.copy('node_modules/font-awesome/fonts', 'public/fonts')
.sass('node_modules/animate.css/animate.css', 'public/css');

